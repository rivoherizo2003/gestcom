### [MAIN IDEA]
- Sales management of stores
- Orders management
- Each store has their employees
   * Each employee has an account

[CUSTOMER SIDE]
- Cashier:
  - [] Billing
  
- Queuing

- Authentication
    - [x] login by email/username and password
    - [x] Remember me
    - [x] Forgot password (Send email with link to reset password)
    - [x] OAuth2 with facebook and google
    - [x] Reset password
    - [x] If there is a valid token redirect to dashboard page 

[ADMIN]
- Manage store's employees:
    - [x] [FRONT & BACKEND] Profile page and Upload picture profile, save profile;
    - [ ] [FRONT & BACKEND] Add new employees:
        - [ ] Create add new form employees;
        - [ ] Check availability username and email;
        - [ ] Upload profile picture;
        - [ ] Validate data on form;
        - [ ] save data person, employee, account for an employee;
    - [ ] [FRONT & BACKEND] List of employees
        - [ ] List employees;
        - [ ] Multi select list of stores to filter list of employees
        - [ ] Delete employee;
          - [ ] show modal confirmation
          - [ ] implement action modal delete

- Manage list items:
    - [ ] List items;
    - [ ] Add new item;
    - [ ] Edit item;
    - [ ] Delete item;

- Manage orders:
    - [ ] List orders;
    - [ ] New orders;
  
- Manage sales:
    - [ ] List sales;

- Dashboard:
    - [ ] sales statistics;
    - [ ] order statistics


- Public API
    - [ ] Create public endpoint for a specific store and its items collection
    - [ ] Create endpoint which creates orders
    - [ ] Endpoint for online payment
    - [ ] List bills