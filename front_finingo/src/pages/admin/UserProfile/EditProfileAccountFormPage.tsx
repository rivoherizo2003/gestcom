import { useState } from "react";
import useHttp from "../../../hooks/use-http";
import OutlineButton from "../../../components/UI/OutlineButton";
import AccountFormContent from "../../../components/Account/AccountFormContent";
import { alterateFormData } from "../../../helpers/account-helpers";

const EditProfileAccountFormPage = (props: AccountFormType) => {
  const { sendRequest: saveAccountInfo } = useHttp();
  const [formAccountIsValid, setFormAccountIsValid] = useState<boolean>(false);
  const [selectedImgProfile, setSelectedImgProfile] = useState<File | null>(
    null
  );

  const handleFormValidationStatus = (status: boolean) => {
    setFormAccountIsValid(() => status);
  };

  const handleSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = alterateFormData({
      account: props.account,
      imgProfile: selectedImgProfile,
      submitEvent: e,
    });

    saveAccountInfo(
      {
        url: process.env["REACT_APP_API_URL"] + "/api/update/account",
        isAuthenticationNeeded: true,
        useFormData: true,
        body: formData,
        method: "PUT",
      },
      () => {
        if (props.handleClickBtnCancel) props.handleClickBtnCancel();
        if (props.handleAfterSubmit) props.handleAfterSubmit();
      }
    );
  };

  return (
    <form onSubmit={handleSubmit}>
      <AccountFormContent
        userAvatar={props.userAvatar}
        account={props.account}
        handleFormValidationStatus={handleFormValidationStatus}
        handleSelectedImgProfile={(imgProfileSelected: File | null) => {
          setSelectedImgProfile(() => imgProfileSelected);
        } }/>

      {formAccountIsValid && (
        <OutlineButton
          type={"submit"}
          className={"btn-outline-success btn-flat float-left"}
        >
          <i className="fas fa-sd-card"></i> Save account
        </OutlineButton>
      )}

      <OutlineButton
        type={"button"}
        onClickHandler={props.handleClickBtnCancel}
        className={"btn-outline-warning btn-flat float-left mt-2"}
      >
        <i className="fas fa-ban"></i> Cancel
      </OutlineButton>
    </form>
  );
};

export default EditProfileAccountFormPage;
