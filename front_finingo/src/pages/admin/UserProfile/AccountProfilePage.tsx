import { useEffect, useRef, useState } from "react";
import EditProfileAccountFormPage from "./EditProfileAccountFormPage";
import CardWidgetUser from "../../../components/UI/CardWidgetUser";
import OutlineButton from "../../../components/UI/OutlineButton";
import Logo from "../../../assets/images/FININGO.png";
import useHttp from "../../../hooks/use-http";

const AccountProfilePage = (props: AccountProfilePageType) => {
  const [isAccountFormVisible, setIsAccountFormVisible] =
    useState<boolean>(false);
  const { isLoading, sendRequest: getUserAvatar } = useHttp();
  const [userAvatar, setUserAvatar] = useState<string>(Logo);
  const shouldLock = useRef(false);

  useEffect(() => {
    if (!shouldLock.current) {
      shouldLock.current = true;
      const transformData = (data: Blob) => {
        setUserAvatar(() => {
          return URL.createObjectURL(data);
        });
      };

      let headers = new Headers({ "Content-Type": "application/json" });
      getUserAvatar(
        {
          url:
            process.env["REACT_APP_API_URL"] +
            "/api/content/media?id=" +
            props.account.media_id,
          isAuthenticationNeeded: true,
          useFormData: false,
          headers: headers,
          body: null,
        },
        transformData
      );
    }
  }, []);

  const handleToggle = () => {
    setIsAccountFormVisible((prev) => !prev);
  };

  const handleAccountStateUpdate = () => {
    props.refreshUserProfileData();
  };

  return (
    <>
      {!isAccountFormVisible && (
        <CardWidgetUser
          isLoading={isLoading}
          isThereBgImg={true}
          userAvatar={userAvatar}
          additionalClasses="shadow-lg"
          fullname={props.fullName}
          userJob={props.account.roles.join(",")}
          descriptionHeader1="Email"
          descriptionText1={props.account.email}
          descriptionHeader2="Username"
          descriptionText2={props.account.username}
          descriptionHeader3="User's code"
          descriptionText3={props.account.user_code}
        />
      )}
      {isAccountFormVisible && (
        <EditProfileAccountFormPage
          userAvatar={userAvatar}
          account={props.account}
          handleClickBtnCancel={handleToggle}
          handleAfterSubmit={handleAccountStateUpdate}
        />
      )}
      {!isAccountFormVisible && (
        <OutlineButton
          type={"button"}
          onClickHandler={handleToggle}
          className={"btn-outline-primary btn-flat float-left"}
        >
          <i className="fa fa-edit"></i> Edit account
        </OutlineButton>
      )}
    </>
  );
};

AccountProfilePage.propTypes = {};

export default AccountProfilePage;
