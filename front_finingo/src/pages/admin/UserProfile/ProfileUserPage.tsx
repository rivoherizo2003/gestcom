import { useEffect, useRef, useState } from "react";
import useHttp from "../../../hooks/use-http";
import AccountProfilePage from "./AccountProfilePage";
import ProfileGeneralInfosPage from "./ProfileGeneralInfosPage";

const ProfileUserPage = () => {
  const [userProfile, setUserProfile] = useState<UserProfile>();
  const [isProfileInitialized, setIsProfileInitialized] = useState<boolean>(false)
  const shouldLock = useRef(false);
  const { isLoading, sendRequest: getUserInfo } = useHttp();
  useEffect(() => {
    if (!shouldLock.current) {
      shouldLock.current = true;
      const transformData = (data: UserProfile) => {
        setUserProfile(() => {
          return data;
        });
        setIsProfileInitialized(() => false);
      };
      let headers = new Headers();
      headers.set("Content-type", "application/json");
      getUserInfo(
        {
          url:
            process.env["REACT_APP_API_URL"] +
            "/api/account-info?id=" +
            localStorage.getItem("idUser"),
          isAuthenticationNeeded: true,
          useFormData: false,
          headers: headers,
          body: null,
        },
        transformData
      );
    }
  }, [isProfileInitialized]);

  const userProfileDataHasChanged = () => {
    shouldLock.current = false;
    setIsProfileInitialized(() => true);
  }

  return (
    <>
      <section className="content pt-2">
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-5 col-sm-3">
                <div
                  className="nav flex-column nav-tabs h-100"
                  id="vert-tabs-tab"
                  role="tablist"
                  aria-orientation="vertical"
                >
                  <a
                    className="nav-link active"
                    id="vert-tabs-profile-tab"
                    data-toggle="pill"
                    href="#vert-tabs-profile"
                    role="tab"
                    aria-controls="vert-tabs-profile"
                    aria-selected="false"
                  >
                    Account
                  </a>
                  <a
                    className="nav-link"
                    id="vert-tabs-messages-tab"
                    data-toggle="pill"
                    href="#vert-tabs-messages"
                    role="tab"
                    aria-controls="vert-tabs-messages"
                    aria-selected="false"
                  >
                    General informations
                  </a>
                </div>
              </div>
              <div className="col-7 col-sm-9">
                <div className="tab-content" id="vert-tabs-tabContent">
                  <div
                    className="tab-pane fade active show"
                    id="vert-tabs-profile"
                    role="tabpanel"
                    aria-labelledby="vert-tabs-profile-tab"
                  >
                    {!isLoading &&
                      userProfile &&
                      null !== userProfile && (
                        <div className="col-6 pb-2">
                          <AccountProfilePage account={userProfile.account} fullName={userProfile.employee.person.firstname+" "+userProfile.employee.person.lastname} refreshUserProfileData={userProfileDataHasChanged}/>
                        </div>
                      )}
                    {isLoading && (
                      <i className="fas fa-spin fa-spinner text-gray"></i>
                    )}
                  </div>
                  <div
                    className="tab-pane fade"
                    id="vert-tabs-messages"
                    role="tabpanel"
                    aria-labelledby="vert-tabs-messages-tab"
                  >
                    {!isLoading &&
                      userProfile &&
                      null !== userProfile && (
                        <ProfileGeneralInfosPage employee={userProfile.employee} refreshUserProfileData={userProfileDataHasChanged}/>
                      )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ProfileUserPage;
