import OutlineButton from "../../../components/UI/OutlineButton";
import EmployeeCardAbout from "../../../components/Employee/EmployeeCardAbout";
import { useState } from "react";
import useHttp from "../../../hooks/use-http";
import PersonFormFields from "../../../components/Person/PersonFormFields";
import EmployeeForm from "../../../components/Employee/EmployeeForm";

const ProfileGeneralInfosPage = (props: ProfileGeneralInfoPageType) => {
  const { sendRequest: sendEmployeeData } = useHttp();
  const [isEmployeeFormVisible, setIsEmployeeFormVisible] =
    useState<boolean>(false);

  const handleToggle = () => {
    setIsEmployeeFormVisible((prev) => !prev);
  };

  const handleAccountStateUpdate = () => {
    props.refreshUserProfileData();
  };

  const handleSubmitProfileEmployee = (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();
    let formDataEmployee = new FormData(e.currentTarget);
    const headers = new Headers({
      "Content-Type": "application/json",
    });

    sendEmployeeData(
      {
        url: process.env["REACT_APP_API_URL"] + "/api/update/employee?id="+props.employee.person.id,
        isAuthenticationNeeded: true,
        useFormData: false,
        body: JSON.stringify(Object.fromEntries(formDataEmployee)),
        method: "PUT",
        headers: headers
      },
      () => {
        handleToggle();
        handleAccountStateUpdate();
      }
    );
  }

  return (
    <>
      {!isEmployeeFormVisible && (
        <>
          <EmployeeCardAbout employee={props.employee} />
          <OutlineButton
            type={"button"}
            onClickHandler={handleToggle}
            className={"btn-outline-warning btn-flat float-left"}
          >
            <i className="fa fa-edit"></i> Edit general informations
          </OutlineButton>
        </>
      )}
      {
        isEmployeeFormVisible && <form onSubmit={handleSubmitProfileEmployee}>
        <PersonFormFields person={props.employee?.person}/>
        <EmployeeForm employee={props.employee}/>
        <OutlineButton
          type={"submit"}
          className={"btn-outline-success btn-flat float-left"}
        >
          <i className="fas fa-sd-card"></i> Save general information
        </OutlineButton>
  
        <OutlineButton
          type={"button"}
          onClickHandler={handleToggle}
          className={"btn-outline-warning btn-flat float-left mt-2"}
        >
          <i className="fas fa-ban"></i> Cancel
        </OutlineButton>
      </form>
      }
    </>
  );
};

export default ProfileGeneralInfosPage;
