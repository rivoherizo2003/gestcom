import RoleProvider from "../../store/RoleProvider";
import RootAdminPage from "../RootAdminPage";

const RootAdminLayout = () => {
  document.body.classList.remove("login-page");
  document.body.classList.remove("background-body");
  document.body.classList.add("hold-transition");
  document.body.classList.add("sidebar-mini");
  document.body.classList.add("layout-fixed");
  document.body.classList.add("sidebar-collapse");
  
  return (
    <RoleProvider>
      <div className="wrapper">
        <RootAdminPage/>
      </div>
    </RoleProvider>
  );
};

export default RootAdminLayout;
