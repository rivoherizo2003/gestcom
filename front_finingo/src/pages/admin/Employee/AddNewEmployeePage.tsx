import { useState } from "react";
import AccountFormContent from "../../../components/Account/AccountFormContent";
import OutlineButton from "../../../components/UI/OutlineButton";
import useHttp from "../../../hooks/use-http";
import Card from "../../../components/UI/Card";
import EmployeeForm from "../../../components/Employee/EmployeeForm";
import { alterateFormData } from "../../../helpers/employee-form-helpers";
import PersonFormFields from "../../../components/Person/PersonFormFields";
import Toast from "../../../components/UI/Toast";
import { ToastClassesEnum } from "../../../components/UI/Enums/toast-enum";

const AddNewEmployeePage = (props: AccountFormType) => {
  const { sendRequest: addNewEmployee } = useHttp();
  const [toastMessage, setToastMessage] = useState<string>("");
  const [toastIsShown, setToastIsShown] = useState<boolean>(false);
  const [toastClass, setToastClass] = useState<ToastClassesEnum>(
    ToastClassesEnum.INFO
  );
  const [formAccountIsValid, setFormAccountIsValid] = useState<boolean>(false);
  const [selectedImgProfile, setSelectedImgProfile] = useState<File | null>(
    null
  );

  const handleFormValidationStatus = (status: boolean) => {
    setFormAccountIsValid(() => status);
  };

  const handleSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = alterateFormData({
      imgProfile: selectedImgProfile,
      submitEvent: e,
    });

    addNewEmployee(
      {
        url: process.env["REACT_APP_API_URL"] + "/api/add/employee",
        isAuthenticationNeeded: true,
        useFormData: true,
        body: formData,
        method: "POST",
      },
      (data) => {
        if (data.error) {
          setToastMessage(() => data.error + " :" + data.message);
          setToastClass(() => ToastClassesEnum.ERROR);
          setToastIsShown(() => true);
        }
        if (props.handleClickBtnCancel) props.handleClickBtnCancel();
        if (props.handleAfterSubmit) props.handleAfterSubmit();
      }
    );
  };

  const handleCloseToast = () => {
    setToastIsShown(() => false);
  };

  return (
    <>
      {toastIsShown && (
        <Toast
          toastTitle={"Error"}
          content={toastMessage}
          bgColor={toastClass}
          closeButton={false}
          showDuration={5000}
          autoHide={true}
          toastIsShown={toastIsShown}
          onClose={handleCloseToast}
        />
      )}

      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-6 col-sm-12 col-md-6 col-xs-6">
            <Card
              cardClasses={"card card-primary"}
              cardTitle={"Account informations"}
            >
              <AccountFormContent
                userAvatar={props.userAvatar}
                account={props.account}
                handleFormValidationStatus={handleFormValidationStatus}
                handleSelectedImgProfile={(imgProfileSelected: File | null) => {
                  setSelectedImgProfile(() => imgProfileSelected);
                }}
              />
            </Card>
          </div>
          <div className="col-6 col-sm-12 col-md-6 col-xs-6">
            <Card
              cardClasses={"card card-info"}
              cardTitle={"Common informations"}
            >
              <PersonFormFields person={null} />
            </Card>
          </div>
        </div>
        <div className="row">
          <div className="col-6 col-sm-12 col-md-6 col-xs-6">
            <Card
              cardClasses={"card card-info"}
              cardTitle={"Employee informations"}
            >
              <EmployeeForm employee={null} />
            </Card>
          </div>
        </div>
        <div className="row pb-5">
          <div className="col-6 col-sm-12 col-md-6 col-xs-6">
            {formAccountIsValid && (
              <OutlineButton
                type={"submit"}
                className={"btn-outline-success btn-flat float-left"}
              >
                <i className="fas fa-sd-card"></i> Add new Employee
              </OutlineButton>
            )}
          </div>
          <div className="col-6 col-sm-12 col-md-6 col-xs-6">
            <OutlineButton
              type={"button"}
              onClickHandler={props.handleClickBtnCancel}
              className={"btn-outline-warning btn-flat float-left"}
            >
              <i className="fas fa-ban"></i> Cancel
            </OutlineButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddNewEmployeePage;
