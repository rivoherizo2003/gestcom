import { Outlet } from "react-router-dom";

const RootAccountsLayout = () => {
  return <Outlet />;
};

export default RootAccountsLayout;
