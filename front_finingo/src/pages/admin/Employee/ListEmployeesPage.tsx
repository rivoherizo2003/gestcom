import { useState } from "react";
import EmployeeSmallCard from "../../../components/Employee/EmployeeSmallCard";
import ButtonApp from "../../../components/UI/ButtonApp";
import AddNewEmployeePage from "./AddNewEmployeePage";
import Avatar from "../../../assets/images/FININGO.png";

const ListEmployeesPage = () => {
  const [isAddAccountClicked, setIsAddAccountClicked] = useState(false);
  const [isListAccountsClicked, setIsListAccountsClicked] = useState(true);
  const [titleHeader, setTitleHeader] = useState("Employees");
  const handleAddAccountClick = () => {
    setIsAddAccountClicked((prev) => {
      return !prev;
    });
    setTitleHeader(() => "Add new employee");
    setIsListAccountsClicked(false);
  };

  const handleListAccountClick = () => {
    setIsListAccountsClicked((prev) => {
      return !prev;
    });
    setTitleHeader(() => "Employees");
    setIsAddAccountClicked(false);
  };

  return (
    <>
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>{titleHeader}</h1>
            </div>
            <div className="col-sm-6">
              {!isAddAccountClicked && (
                <ButtonApp
                  bgClass={"bg-success float-right"}
                  iconClasses={"fas fa-user-plus"}
                  caption={"Add"}
                  onClickHandler={handleAddAccountClick}
                />
              )}
              {!isListAccountsClicked && (
                <ButtonApp
                  bgClass={"bg-secondary float-right"}
                  iconClasses={"fas fa-list"}
                  caption={"List"}
                  onClickHandler={handleListAccountClick}
                />
              )}
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <>
          {isAddAccountClicked && <AddNewEmployeePage userAvatar={Avatar} account={null} />}
          {isListAccountsClicked && (
            <div className="card card-solid">
              <div className="card-body pb-0">
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                    <EmployeeSmallCard />
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <nav aria-label="Contacts Page Navigation">
                  <ul className="pagination justify-content-center m-0">
                    <li className="page-item active">
                      <a className="page-link" href="#">
                        1
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        2
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        3
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        4
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        5
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        6
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        7
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="#">
                        8
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          )}
        </>
      </section>
    </>
  );
};
export default ListEmployeesPage;
