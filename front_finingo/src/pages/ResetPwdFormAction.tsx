import { LoaderFunctionArgs } from "react-router-dom";

export async function ResetPwdFormAction(loader: LoaderFunctionArgs) {
  const data = await loader.request.formData();
  const token = localStorage.getItem("tokenResetPwd");
  localStorage.removeItem("tokenResetPwd");
  const resetPwdData = {
    password: data.get("password"),
  };

  const response = await fetch(
    process.env["REACT_APP_API_URL"] + "/api/security/reset-password",
    {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(resetPwdData),
    }
  );

  if (response.status === 422 || response.status === 401) {
    return response;
  }

  const responseData = await response.json();

  return responseData;
}
