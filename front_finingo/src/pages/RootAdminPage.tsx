import { Outlet } from "react-router-dom";
import MainNavigation from "../components/Navigation/MainNavigation";
import NavBar from "../components/Navigation/NavBar";
import { useContext, useEffect } from "react";
import useHttp from "../hooks/use-http";
import RoleContext from "../store/role-context";

const RootAdminPage = () => {


    const {sendRequest: getPredefinedList} = useHttp();
    const roleAccountCtx = useContext(RoleContext);
  
    const addRoles = (roles: string[]) => {
      roleAccountCtx.addRoles(roles);
    }
  
    useEffect(() => {
      getPredefinedList(
        {
          url: process.env['REACT_APP_API_URL']+"/api/list",
          isAuthenticationNeeded: true,
          useFormData:false,
          method: 'GET',
          body:null
        },
        (data: DataListType) => {
          addRoles(data.roles);
        }
      )
    }, []);
    
  return (
    <>
      <NavBar />
      <MainNavigation />
      <div className="content-wrapper">
        <Outlet />
      </div>
    </>
  );
};

export default RootAdminPage;
