import classes from "./LoginPage.module.css";
import Card from "../components/UI/Card";
import ResetPwdForm from "./ResetPwdForm";

const ResetPwdPage = () => {
  document.body.classList.add("login-page");
  document.body.classList.add("background-body");
  const loginBoxClasses = "login-box " + classes["login-cust"];

  return (
    <div className={loginBoxClasses}>
      <Card cardClasses="card-outline card-primary" cardTitle="Finingo">
        <p className="login-box-msg">Reset password</p>
        <ResetPwdForm />
      </Card>
    </div>
  );
};

export default ResetPwdPage;
