import { redirect } from "react-router-dom";

export function LogoutLoader() {
  localStorage.removeItem("token");
  localStorage.removeItem("expiration");

  return redirect("/");
}
