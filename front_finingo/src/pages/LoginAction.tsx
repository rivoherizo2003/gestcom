import jwtDecode from "jwt-decode";
import { json, redirect, LoaderFunctionArgs } from "react-router-dom";

export async function LoginAction(loader: LoaderFunctionArgs) {
  const data = await loader.request.formData();
  const authData = {
    usernameOrEmail: data.get("usernameOrEmail"),
    password: data.get("password"),
    isMeRemembered: "on" === data.get("isMeRemembered") ? "on" : "off",
  };
  const response = await fetch(
    process.env["REACT_APP_API_URL"] + "/api/security/authenticate",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(authData),
    }
  );

  if (response.status === 422 || response.status === 401) {
    return response;
  }

  if (!response.ok) {
    throw json(
      {
        message: "Could not authenticate user.",
      },
      {
        status: 500,
      }
    );
  }

  const resData = await response.json();
  const token = resData.accessToken;
  const username = resData.usernameOrEmail;
  const fullName = resData.fullName;
  const profilePath = resData.profilePath;

  localStorage.setItem("token", token);
  localStorage.setItem("username", username);
  localStorage.setItem("fullName", fullName);
  localStorage.setItem("profilePath", profilePath);
  localStorage.setItem("isMeRemembered", authData.isMeRemembered);
  localStorage.setItem("idUser", resData.id);
  const tokenData = jwtDecode<MyToken>(token);
  localStorage.setItem("expiration", tokenData.exp);

  return redirect("/admin/dashboard");
}
