import { NavLink } from "react-router-dom";
import classes from "./LoginPage.module.css";
import ForgotPasswordForm from "../components/Auth/ForgotPasswordForm";

const ForgotPasswordPage = () => {
  document.body.classList.add("login-page");
  document.body.classList.add("background-body");
  const loginBoxClasses = "login-box " + classes["login-cust"];

  return (
    <>
      <div className={loginBoxClasses}>
        <div className="login-logo">
          <NavLink to="/">
            <b>Finingo</b>
          </NavLink>
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            {<ForgotPasswordForm />}

            <p className="mt-3 mb-1">
              <NavLink to="/">
                <i className="fas fa-chevron-left"></i> Back to authentication{" "}
              </NavLink>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default ForgotPasswordPage;
