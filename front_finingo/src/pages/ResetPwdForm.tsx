import React, { useEffect, useRef, useState } from "react";
import { Form, useActionData, useNavigate } from "react-router-dom";
import Input from "../components/UI/Input";
import { isTapedPasswordValid } from "../helpers/auth-helpers";
import { LOCK_ICON } from "../constants/iconAwesome";

const ResetPwdForm = () => {
  const navigate = useNavigate();
  const data = (useActionData() as ApiResponseType) ?? null;
  const passwordRef = useRef<HTMLInputElement | null>(null);
  const confirmedPasswordRef = useRef<HTMLInputElement | null>(null);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isConfirmedPasswordValid, setIsConfirmedPasswordValid] =
    useState(true);
  const [isResetPwdSuccessfull, setIsResetPwdSuccessfull] = useState(false);

  const confirmPasswordOnChangeHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsConfirmedPasswordValid(
      event.target.value === passwordRef?.current?.value
    );
  };

  const passwordOnChangeHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsConfirmedPasswordValid(
      event.target.value === confirmedPasswordRef?.current?.value
    );
    const password = passwordRef?.current?.value;
    if (password !== undefined)
      setIsPasswordValid(isTapedPasswordValid(password));
    else setIsPasswordValid(false);
  };

  const isbtnResetDisabled =
    isConfirmedPasswordValid && isPasswordValid ? false : true;

  const passwordClasses = isPasswordValid
    ? "form-control"
    : "form-control is-invalid";

  const passwordConfirmClasses = isConfirmedPasswordValid
    ? "form-control"
    : "form-control is-invalid";

  useEffect(() => {
    setIsResetPwdSuccessfull(() => {
      return data !== null ? data.success : false;
    });
  }, [data]);

  if (isResetPwdSuccessfull) {
    setTimeout(() => {
      navigate("/");
    }, 3000);
  }

  return (
    <>
      {isResetPwdSuccessfull &&
        data !== null &&
        data.success &&
        data.message && (
          <div className="input-group mb-3">
            <ul>
              <li className="text-success">{data.message}</li>
            </ul>
          </div>
        )}
      {!isResetPwdSuccessfull && (
        <Form method="post">
          {data !== null && !data.success && (
            <div className="input-group mb-3">
              <ul>
                <li className="text-danger">{data.message}</li>
              </ul>
            </div>
          )}
          <div className="input-group mb-3">
            <Input
              type="password"
              Inputclasses={passwordClasses}
              placeholder={"Type new password"}
              isRequired={true}
              name="password"
              ref={passwordRef}
              defaultValue="QWqw@123"
              onChangeHandler={passwordOnChangeHandler}
              icon={LOCK_ICON}
            />
            <span className="error invalid-feedback">
              Password should include lowercase and uppercase letter, number,
              special character{" "}
            </span>
          </div>

          <div className="input-group mb-3">
            <Input
              type="password"
              Inputclasses={passwordConfirmClasses}
              placeholder="Re-type new password"
              isRequired={true}
              name="confirm-pwd"
              ref={confirmedPasswordRef}
              defaultValue="QWqw@123"
              onChangeHandler={confirmPasswordOnChangeHandler}
              icon={LOCK_ICON}
            />
            <span className="error invalid-feedback">
              New password not confirmed
            </span>
          </div>

          <div className="row">
            <div className="col-sm">
              <button
                type="submit"
                disabled={isbtnResetDisabled}
                className="btn btn-warning btn-block"
              >
                Reset password
              </button>
            </div>
          </div>
        </Form>
      )}
    </>
  );
};

export default ResetPwdForm;
