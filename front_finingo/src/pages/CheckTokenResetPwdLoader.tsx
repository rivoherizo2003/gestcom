import { LoaderFunctionArgs, redirect } from "react-router-dom";
import { validateToken } from "../services/Account/ValidateUserToken";

export async function CheckTokenResetPwdLoader(loader: LoaderFunctionArgs) {
  const urlData = loader.request.url?.split("?");
  if (urlData !== undefined && urlData.length > 1) {
    const urlTokenData = urlData[1]?.split("=");
    if (urlTokenData !== undefined) {
      const tokenReset = urlTokenData[1] ?? "";
      localStorage.setItem("tokenResetPwd", tokenReset);

      //if not expired => re validate token
      const ret = await validateToken(tokenReset);

      if (ret.success) {
        return null;
      }
    }
  }

  return redirect("/");
}
