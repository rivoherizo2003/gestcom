import Card from "../components/UI/Card";
import classes from "./LoginPage.module.css";
import AuthForm from "../components/Auth/AuthForm";
import { FACEBOOK_AUTH_URL, GOOGLE_AUTH_URL } from "../constants/auth";
import { NavLink } from "react-router-dom";

const LoginPage = () => {
  document.body.classList.add("login-page");
  document.body.classList.add("background-body");
  const loginBoxClasses = "login-box " + classes["login-cust"];
  const isMeRemembered: boolean =
    null !== localStorage.getItem("isMeRemembered");
  localStorage.removeItem("tokenResetPwd");
  const usernameOrEmail: string | null = localStorage.getItem("username");

  return (
    <div className={loginBoxClasses}>
      <Card
        cardClasses="card-outline card-primary"
        cardTitle="Finingo"
        cardHeaderClasses="text-center"
      >
        <p className="login-box-msg">Authentication</p>

        <AuthForm
          usernameOrEmail={usernameOrEmail}
          isMeRemembered={isMeRemembered}
        />

        <div className="social-auth-links text-center mt-2 mb-3">
          <a href={FACEBOOK_AUTH_URL} className="btn btn-block btn-primary">
            <i className="fab fa-facebook mr-2"></i> Sign in using Facebook
          </a>
          <a href={GOOGLE_AUTH_URL} className="btn btn-block btn-danger">
            <i className="fab fa-google-plus mr-2"></i> Sign in using Google+
          </a>
        </div>

        <p className="mb-1">
          <NavLink to="/forgot-password">I forgot my password</NavLink>
        </p>
        {/* <p className="mb-0">
          <a href="register.html" className="text-center">
            Register a new membership
          </a>
        </p> */}
      </Card>
    </div>
  );
};

export default LoginPage;
