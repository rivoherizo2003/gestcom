import { LoaderFunctionArgs } from "react-router-dom";

export async function ForgotPasswordAction(loader: LoaderFunctionArgs) {
  const data = await loader.request.formData();
  const forgotData = {
    email: data.get("email"),
  };
  const response = await fetch(
    process.env["REACT_APP_API_URL"] + "/api/security/request-new-password",
    {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(forgotData),
    }
  );
  const responseData = await response.json();

  if (response.status === 422 || response.status === 401) {
    return response;
  }

  return responseData;
}
