import { GENDER_MAN } from "../../constants/person";
import Card from "../UI/Card";

interface EmployeeCardAboutType {
  employee: Employee;
}

const EmployeeCardAbout = (props: EmployeeCardAboutType) => {
  let title: string =
    props.employee.person.gender === GENDER_MAN ? "Mr." : "Ms.";
  title +=
    " " +
    props.employee.person.firstname +
    " " +
    props.employee.person.lastname;

  return (
    <Card cardClasses="card-primary" cardBodyClasses="" cardTitle={title}>
      <strong>
        <i className="fas fa-phone mr-1"></i> Phone number
      </strong>

      <p className="text-muted">{props.employee.person.phoneNumber}</p>

      <hr />

      <strong>
        <i className="fas fa-book mr-1"></i> Address
      </strong>

      <p className="text-muted">{props.employee.person.address}</p>

      <hr />

      <strong>
        <i className="fas fa-book mr-1"></i> Education
      </strong>

      <p className="text-muted">{props.employee.education}</p>

      <hr />

      <strong>
        <i className="fas fa-book mr-1"></i> Position title
      </strong>

      <p className="text-muted">{props.employee.positionTitle}</p>

      <hr />

      <strong>
        <i className="fas fa-map-marker-alt mr-1"></i> Location
      </strong>

      <p className="text-muted">{props.employee.person.address}</p>

      <hr />

      <strong>
        <i className="fas fa-pencil-alt mr-1"></i> Skills
      </strong>

      <p className="text-muted">{props.employee.skills}</p>

      <hr />

      <strong>
        <i className="far fa-file-alt mr-1"></i> Notes
      </strong>

      <p className="text-muted">{props.employee.comment}</p>
    </Card>
  );
};

export default EmployeeCardAbout;
