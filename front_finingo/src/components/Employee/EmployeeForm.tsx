import TextareaFormGroup from "../UI/TextareaFormGroup";
import InputFormGroup from "../UI/InputFormGroup";
import { USER_ICON } from "../../constants/iconAwesome";

const EmployeeForm = (props: EmployeeFormType) => {

  return (
    <>
      <InputFormGroup
        labelTitle="Position title:"
        type="text"
        Inputclasses="form-control"
        placeholder="Position title"
        defaultValue={props.employee?.positionTitle}
        name="position_title"
        id="text-position-title"
        icon={USER_ICON}
        hasError={false}
        isRequired={false}
      />

      <TextareaFormGroup
        id="textarea-skills"
        name="skills"
        rows={3}
        placeholder="Skills"
        labelTitle="Skills:"
        labelId="lbl_textarea_skills"
        defaultValue={props.employee?.skills}
      />

      <TextareaFormGroup
        id="textarea-education"
        rows={3}
        placeholder="Education"
        labelTitle="Education:"
        labelId="lbl_textarea_education"
        name="education"
        defaultValue={props.employee?.education}
      />

      <TextareaFormGroup
        id="textarea-comment"
        name="comment"
        rows={3}
        placeholder="Comments:"
        labelTitle="Comments:"
        labelId="lbl_comments"
        defaultValue={props.employee?.comment}
      />
    </>
  );
};

export default EmployeeForm;
