import Card from "../UI/Card";
import Avatar from "../../assets/images/accounts/user2-160x160.jpg";

const EmployeeSmallCard = () => {
  const cardFooterContent = () => {
    return (
      <>
        <div className="text-right">
          <a href="#" className="btn btn-sm bg-teal">
            <i className="fas fa-comments"></i>
          </a>
          <a href="#" className="btn btn-sm btn-primary">
            <i className="fas fa-user"></i> View employee
          </a>
        </div>
      </>
    );
  };

  return (
    <Card
      cardTitle={"Employee information"}
      cardClasses={"bg-light d-flex flex-fill"}
      cardBodyClasses={"pt-0"}
      cardHeaderClasses="text-muted border-bottom-0"
      footerElement={cardFooterContent()}
    >
      <div className="row">
        <div className="col-7">
          <h2 className="lead">
            <b>Nicole Pearson</b>
          </h2>
          <p className="text-muted text-sm">
            <b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover{" "}
          </p>
          <ul className="ml-4 mb-0 fa-ul text-muted">
            <li className="small">
              <span className="fa-li">
                <i className="fas fa-lg fa-building"></i>
              </span>{" "}
              Address: Demo Street 123, Demo City 04312, NJ
            </li>
            <li className="small">
              <span className="fa-li">
                <i className="fas fa-lg fa-phone"></i>
              </span>{" "}
              Phone #: + 800 - 12 12 23 52
            </li>
          </ul>
        </div>
        <div className="col-5 text-center">
          <img
            src={Avatar}
            alt="user-avatar"
            className="img-circle img-fluid"
          />
        </div>
      </div>
    </Card>
  );
};

export default EmployeeSmallCard;
