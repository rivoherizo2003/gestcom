import Card from "../UI/Card";
import Logo from "../../assets/images/FININGO.png";
import { useEffect, useState } from "react";

interface UserProfileCardType{
  userProfile: UserProfile;
  handleEditProfileClick: () => void;
}

const UserProfileCard = (props: UserProfileCardType) => {
  const userProfile = props.userProfile;
  const person: Person = userProfile.employee.person;
  const [profilePath, setProfilePath] = useState(Logo);
  useEffect(() => {
    setProfilePath(() => userProfile.account.media_id??Logo);
  },[userProfile]);

  return (
    <Card cardClasses="card-primary card-outline" cardBodyClasses="box-profile">
      <div className="text-center">
        <img
          className="profile-user-img img-fluid img-circle"
          src={profilePath}
          alt="User profile picture"
        />
      </div>

      <h3 className="profile-username text-center">
        {person.firstname + " " + person.lastname}
      </h3>

      <p className="text-muted text-center">{userProfile.employee.positionTitle}</p>

      <ul className="list-group list-group-unbordered mb-3">
        <li className="list-group-item">
          <b>Email</b> <a className="float-right">{userProfile.account.email}</a>
        </li>
        <li className="list-group-item">
          <b>Tel. number</b> <a className="float-right">{person.phoneNumber}</a>
        </li>
        <li className="list-group-item">
          <b>Sales</b> <a className="float-right">13,287</a>
        </li>
      </ul>

      <a href="#" className="btn btn-primary btn-block" onClick={props.handleEditProfileClick}>
        <b>
          Edit <i className="fa fa-edit"></i>
        </b>
      </a>
    </Card>
  );
};

export default UserProfileCard;
