import { useState } from "react";
import { previewFile } from "../../helpers/file-helpers";
import InputFile from "../UI/InputFile";


const UploadUserProfile = (props: UploadUserProfileType) => {
  const [error, setError] = useState({ message: "", isSuccessfull: true });

  const onChangeProfile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (null !== event.target.files && undefined !== event.target.files[0]) {
      if (null !== event.target.files && undefined !== event.target.files[0]) {
        const file = event.target.files[0];
        const fileSize = file.size > 0 ? Math.round(file.size / 1024) : 0;
        if (fileSize === 0) {
          setError(() => {
            return {
              message: "File size cannot be equal to zero",
              isSuccessfull: false,
            };
          });
          props.checkPicProfileValid(false);
        }
  
        if (fileSize > props.maxFileSize) {
          setError(() => {
            return {
              message: "File size cannot exceed more than 1MB",
              isSuccessfull: false,
            };
          });
          props.checkPicProfileValid(false);
        }
  
        if (fileSize > 0 && fileSize <= props.maxFileSize) {
          setError(() => {
            return {
              message: "",
              isSuccessfull: true,
            };
          });
          props.checkPicProfileValid(true);
        }
      }
      previewFile(
        event.target.files[0],
        document.getElementById("img-preview") as HTMLImageElement
      );
      props.handleImgProfile(event);
    }
  };

  return (
    <>
      <div className="input-group col-6">
        <div className="text-center">
          <img
            id="img-preview"
            className="profile-user-img img-fluid img-circle"
            src={props.userAvatar}
            alt="User profile picture"
          />
        </div>
      </div>
      <div className="input-group col-6 pt-2">
        <InputFile
          name=""
          id="file-user"
          titleFileLabel={props.profileFileName}
          classSpanGroupAppend="input-group-text"
          isRequired={false}
          onChangeHandler={onChangeProfile}
        />
        {error && !error.isSuccessfull && (
        <div className="input-group mb-3 text-danger">
          {error.message}
        </div>
      )}
      </div>
    </>
  );
};

export default UploadUserProfile;
