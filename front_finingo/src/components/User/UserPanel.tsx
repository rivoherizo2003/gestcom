import Logo from "../../assets/images/FININGO.png";
import { NavLink } from "react-router-dom";

const UserPanel = () => {
  const fullName = localStorage.getItem("fullName");
  const profilePath = localStorage.getItem("profilePath");

  return (
    <>
      <div className="user-panel mt-3 pb-3 mb-3 d-flex">
        <div className="image">
          <img
            src={profilePath ? Logo : profilePath}
            className="img-circle elevation-2"
            alt="User Image"
          />
        </div>
        <div className="info">
          <NavLink to="profile/user" className="d-block">
            {fullName} <i className="fa fa-edit"></i>
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default UserPanel;
