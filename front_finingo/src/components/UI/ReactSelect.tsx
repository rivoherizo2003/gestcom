import { useState } from "react";
import DivFormGroup from "./DivFormGroup";
import Select from "react-select";

const ReactSelect = (props: ReactSelectType) => {
  const [selectedOptions, setSelectedOptions] = useState(props.defaultValue);
  const handleSelectOnChange = (selectedValues:any) => {
    setSelectedOptions(() => selectedValues);
  }
  

  return (
    <DivFormGroup {...props.divFormGroup}>
      {props.labelTitle && <label>{props.labelTitle}</label>}
      <Select
        name={props.nameInputHidden}
        id={props.id}
        value={selectedOptions}
        isMulti={props.isMulti}
        className="basic-multi-select"
        classNamePrefix="select"
        options={props.options}
        onChange={handleSelectOnChange}
      />
    </DivFormGroup>
  );
};
export default ReactSelect;
