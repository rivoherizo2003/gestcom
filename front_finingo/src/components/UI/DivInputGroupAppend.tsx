import { PropsWithChildren } from "react";

const DivInputGroupAppend = (
  props: PropsWithChildren<DivInputGroupAppendType>
) => {
  return (
    <div className={props.classGroupAppend}>
      <span className="input-group-text">{props.children}</span>
    </div>
  );
};

export default DivInputGroupAppend;
