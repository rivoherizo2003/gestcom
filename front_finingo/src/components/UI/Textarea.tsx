const Textarea = (props: TextAreaType) => {
  return (
    <textarea
      id={props.id}
      className="form-control"
      rows={props.rows}
      placeholder={props.placeholder}
      defaultValue={props.defaultValue}
      name={props.name}
    ></textarea>
  );
};

export default Textarea;
