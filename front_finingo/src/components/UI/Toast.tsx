// import { useState } from "react";
import { ToastType } from "./Types/ToastType";

const Toast = (props: ToastType) => {
  // const [showClass, setShowClass] = useState<string>(props.toastIsShown?"show":"");
  const position = props.position ?? "toasts-top-right";
  const handleCloseClick = () => {
    props.onClose();
    // setShowClass(() => '');
  };

  
  if (props.autoHide) {
    setTimeout(() => {
        handleCloseClick();
    }, props.showDuration);
  }

  return (
    <div id="toastsContainerTopRight" className={position + ` fixed`}>
      <div className={`toast ` + props.bgColor + ` fade show`}>
        <div className="toast-header">
          <strong className="mr-auto">{props.toastTitle}</strong>
          {props.subtitle && <small>{props.subtitle}</small>}
          <button
            type="button"
            onClick={handleCloseClick}
            className="ml-2 mb-1 close"
            aria-label="Close"
          >
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="toast-body">{props.content}</div>
      </div>
    </div>
  );
};

export default Toast;
