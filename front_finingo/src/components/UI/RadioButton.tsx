
const RadioButton = (props: InputRadioType) => {
  return (
    <>
      <input
        type="radio"
        className={props.Inputclasses}
        placeholder={props.placeholder}
        name={props.name}
        defaultChecked= {props.isChecked}
        ref={props.ref}
        id={props.id}
        value={props.value}
      />
    </>
  );
};

export default RadioButton;
