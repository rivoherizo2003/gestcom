import { PropsWithChildren } from "react";

const OutlineButton = (props:PropsWithChildren<ButtonType>) => {
    return (
        <>
            <button type={props.type} onClick={props.onClickHandler} className={`btn btn-block ${props.className}`}>{props.children}</button>
        </>
    );
};

export default OutlineButton;