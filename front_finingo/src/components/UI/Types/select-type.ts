interface SelectType {
  name?: string;
  id: string;
  defaultValue?: SelectOptionType[] | undefined;
  isMulti: boolean;
  options: SelectOptionType[] | [];
  labelTitle: string;
}

interface ReactSelectType extends SelectType {
  divFormGroup: DivFormGroupType;
  nameInputHidden: string;
}

interface SelectOptionType{
  value: string;
  label: string;  
}
