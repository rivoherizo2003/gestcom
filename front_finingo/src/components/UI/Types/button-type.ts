interface ButtonType {
  type: "button" | "submit" | "reset" | undefined;
  onClickHandler?: any;
  className: string;
}

interface ButtonAppType {
  bgClass: string;
  badgeClasses?: string;
  badgeCaption?: string;
  iconClasses: string;
  caption: string;
  onClickHandler?: any;
}
