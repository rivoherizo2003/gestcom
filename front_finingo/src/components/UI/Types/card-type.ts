interface CardDataType {
  cardClasses: string;
  cardBodyClasses?: string;
  cardTitle?: string;
  cardHeaderClasses?: string;
  footerElement?: React.ReactElement | null;
}

interface CardWidgetUserType{
  isLoading: boolean;
  additionalClasses?: string;
  isThereBgImg: boolean;
  fullname: string;
  userJob: string|null;
  userAvatar?: string;
  descriptionHeader1?: string|null;
  descriptionText1?: string|null;
  descriptionHeader2?: string|null;
  descriptionText2?: string|null;
  descriptionHeader3?: string|null;
  descriptionText3?: string|null;
}
