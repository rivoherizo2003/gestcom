interface IconAwesomeType {
  classes: string;
}

interface InputType {
  type: "text" | "number" | "date" | "email" | "radio" | "file" | "password";
  isRequired: boolean;
  Inputclasses: string;
  placeholder?: string | undefined;
  name?: string | undefined;
  defaultValue?: any;
  ref?: React.LegacyRef<HTMLInputElement> | null;
  id?: string | undefined;
  value?: string | number | undefined;
  onChangeHandler?: any;
  onBlurHandler?: any;
  icon?: string;
}

interface InputFileType {
  id?: string;
  name?: string;
  isRequired: boolean;
  titleFileLabel?: string;
  classSpanGroupAppend: string;
  onChangeHandler?: (file: React.ChangeEvent<HTMLInputElement>) => void;
  icon?: IconAwesomeType;
}

interface InputRadioType extends InputType {
  isChecked: boolean;
}

interface CustomRadioType extends InputRadioType {
  value: string | number;
  labelRadio: string;
}

interface TextAreaType {
  id: string | undefined;
  rows: number;
  placeholder: string | undefined;
  defaultValue: string | number | readonly string[] | undefined;
  name: string;
}
