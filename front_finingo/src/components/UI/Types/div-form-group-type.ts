
interface DivFormGroupType{
    divFormGroupClasses?: string | undefined;
    hasError?:boolean;
    inputId?:string | undefined;
    errorMessage?:string | undefined | null;
}