interface InputFormGroupType extends InputType {
  divFormGroupClasses?: string;
  hasError: boolean;
  errorMessage?: string | null;
  labelTitle?: string;
  options?: Object[] | [];
}

interface TextareaFormGroupType extends TextAreaType{
  divFormGroupClasses?: string;
  labelId: string;
  labelTitle: string;
}
