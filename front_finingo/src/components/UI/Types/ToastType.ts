import { ToastClassesEnum } from "../Enums/toast-enum";

export interface ToastType{
    toastTitle: string;
    subtitle?: string;
    content: string;
    bgColor: ToastClassesEnum;
    position?: string;
    closeButton: boolean;
    showDuration: number;
    autoHide: boolean;
    onClose: () => void;
    toastIsShown: boolean;
}