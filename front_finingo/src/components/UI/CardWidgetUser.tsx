import BgCardWidget from "../../assets/images/photo1.png";

const CardWidgetUser = (props: CardWidgetUserType) => {
  const additionalClasses = props.additionalClasses ?? "shadow-lg";
  let stylesCardBg = {};
  if (props.isThereBgImg) {
    stylesCardBg = { background: "url(" + BgCardWidget + ") center center" };
  }

  return (
    <div className={`card card-widget widget-user ` + additionalClasses}>
      <div className="widget-user-header text-white" style={stylesCardBg}>
        <h3 className="widget-user-username text-right">{props.fullname}</h3>
        <h5 className="widget-user-desc text-right">{props.userJob}</h5>
      </div>
      <div className="widget-user-image">
        {!props.isLoading && (
          <img
            className="img-circle"
            src={props.userAvatar}
            alt="User Avatar"
          />
        )}
        {props.isLoading && (
          <i className="fas fa-spin fa-spinner text-gray"></i>
        )}
      </div>
      <div className="card-footer">
        <div className="row">
          <div className="col-sm-4 border-right">
            <div className="description-block">
              <h5 className="description-header">{props.descriptionHeader1}</h5>
              <span className="description-text text-lowercase">
                {props.descriptionText1}
              </span>
            </div>
          </div>
          <div className="col-sm-4 border-right">
            <div className="description-block">
              <h5 className="description-header">{props.descriptionHeader2}</h5>
              <span className="description-text text-lowercase">
                {props.descriptionText2}
              </span>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="description-block">
              <h5 className="description-header">{props.descriptionHeader3}</h5>
              <span className="description-text text-lowercase">
                {props.descriptionText3}
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardWidgetUser;
