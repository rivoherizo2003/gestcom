import Textarea from "./Textarea";
import DivFormGroup from "./DivFormGroup";

const TextareaFormGroup = (props: TextareaFormGroupType) => {
  return (
    <DivFormGroup divFormGroupClasses={props.divFormGroupClasses}>
      <label htmlFor={props.labelId}>{props.labelTitle}</label>
      <Textarea
        name={props.name}
        id={props.id}
        rows={props.rows}
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
      />
    </DivFormGroup>
  );
};

export default TextareaFormGroup;
