import RadioButton from "./RadioButton";

const CustomRadio = (props: CustomRadioType) => {
  
  return (
    <div className="custom-control custom-radio">
      <RadioButton
        type="radio"
        Inputclasses={props.Inputclasses}
        isChecked={props.isChecked}
        name={props.name}
        id={props.id}
        value={props.value}
        isRequired={false}
      />
      <label htmlFor={props.id} className="custom-control-label">
        {props.labelRadio}
      </label>
    </div>
  );
};

export default CustomRadio;
