import DivInputGroupAppend from "./DivInputGroupAppend";


const InputFile = (props: InputFileType) => {
  return (
    <>
      <div className="custom-file">
        <input
          type="file"
          required={props.isRequired}
          name={props.name}
          id={props.id}
          onChange={props.onChangeHandler}
        />
        {props.titleFileLabel && (
          <label className="custom-file-label" htmlFor={props.id}>
            {props.titleFileLabel}
          </label>
        )}
      </div>
      {props.icon && (
        <DivInputGroupAppend classGroupAppend={props.classSpanGroupAppend} />
      )}
      
    </>
  );
};

export default InputFile;
