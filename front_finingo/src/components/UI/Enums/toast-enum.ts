export enum ToastClassesEnum {
    INFO = "bg-info",
    SUCCESS = "bg-success",
    WARNING = "bg-warning",
    ERROR = "bg-danger"
}
