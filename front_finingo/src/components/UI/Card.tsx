import { PropsWithChildren } from "react";
import CardFooter from "./CardFooter";

const Card = (props: PropsWithChildren<CardDataType>) => {
  const cardClasses = "card " + props.cardClasses;
  const cardBodyClasses = "card-body " + props.cardBodyClasses;
  const titlePosition =
    props.cardHeaderClasses === undefined ? "" : props.cardHeaderClasses;

  return (
    <>
      <div className={cardClasses}>
        {props.cardTitle && (
          <div className={`card-header ${titlePosition}`}>
            {props.cardTitle}
          </div>
        )}
        <div className={cardBodyClasses}>{props.children}</div>
        {props.footerElement && <CardFooter>{props.footerElement}</CardFooter>}
      </div>
    </>
  );
};

export default Card;
