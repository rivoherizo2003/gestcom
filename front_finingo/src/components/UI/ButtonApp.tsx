const ButtonApp = (props: ButtonAppType) => {
  return (
    <a
      className={`btn btn-app ${props.bgClass}`}
      onClick={props.onClickHandler}
    >
      {props.badgeCaption && (
        <span className={`badge ${props.badgeClasses}`}>
          {props.badgeCaption}
        </span>
      )}
      <i className={props.iconClasses}></i> {props.caption}
    </a>
  );
};
export default ButtonApp;
