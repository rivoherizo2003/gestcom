import DivInputGroupAppend from "./DivInputGroupAppend";

const Input = (props: InputType) => {
  const onChangeHandler = props.onChangeHandler ? props.onChangeHandler : null;
  const onBlurHandler = props.onBlurHandler ? props.onBlurHandler : null;

  return (
    <>
      <input
        type={props.type}
        required={props.isRequired}
        className={props.Inputclasses}
        placeholder={props.placeholder}
        name={props.name}
        defaultValue={props.defaultValue}
        ref={props.ref}
        id={props.id}
        value={props.value}
        onChange={onChangeHandler}
        onBlur={onBlurHandler}
      />
      {props.icon && (
        <DivInputGroupAppend classGroupAppend={"input-group-append"}>
          <i className={props.icon}></i>
        </DivInputGroupAppend>
      )}
    </>
  );
};
export default Input;
