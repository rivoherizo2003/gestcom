import Input from "./Input";
import DivFormGroup from "./DivFormGroup";

const InputFormGroup = (attr: InputFormGroupType) => {
  const divFormGroup: DivFormGroupType = {
    divFormGroupClasses: attr.divFormGroupClasses,
    hasError: attr.hasError,
    errorMessage: attr.errorMessage,
    inputId: attr.id,
  };
  return (
    <DivFormGroup {...divFormGroup}>
      {attr.labelTitle && <label htmlFor={attr.id}>{attr.labelTitle}</label>}
      <div className="input-group">{attr && <Input {...attr} />}</div>
    </DivFormGroup>
  );
};

export default InputFormGroup;
