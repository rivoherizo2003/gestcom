import { PropsWithChildren } from "react";

const DivFormGroup = (props: PropsWithChildren<DivFormGroupType>) => {
  const additionnalClassesFormGroup =
    props.divFormGroupClasses !== undefined ? props.divFormGroupClasses : "";
  return (
    <div className={`form-group ${additionnalClassesFormGroup}`}>
      {props.children}
      {props.hasError && (
        <p id={`${props.inputId}-error`} className="error invalid-feedback">
          {props.errorMessage}
        </p>
      )}
    </div>
  );
};

export default DivFormGroup;
