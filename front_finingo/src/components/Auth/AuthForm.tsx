import { Form, useActionData } from "react-router-dom";

const AuthForm = (props: AuthFormtype) => {
  const data = useActionData() as ApiResponseType;

  return (
    <>
      <Form method="post">
        {data && !data.success && (
          <div className="input-group mb-3">
            <ul>
              <li>{data.message}</li>
            </ul>
          </div>
        )}

        <div className="input-group mb-3">
          <input
            type="text"
            required
            defaultValue={
              null === props.usernameOrEmail ? "" : props.usernameOrEmail
            }
            className="form-control"
            name="usernameOrEmail"
            placeholder="Email or username"
          />
          <div className="input-group-append">
            <div className="input-group-text">
              <span className="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div className="input-group mb-3">
          <input
            type="password"
            name="password"
            defaultValue="admin123456"
            required
            className="form-control"
            placeholder="Password"
          />
          <div className="input-group-append">
            <div className="input-group-text">
              <span className="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-8">
            <div className="icheck-primary">
              <input
                type="checkbox"
                id="remember"
                defaultChecked={props.isMeRemembered}
                name="isMeRemembered"
              />
              <label htmlFor="remember">Remember Me</label>
            </div>
          </div>
          <div className="col-4">
            <button type="submit" className="btn btn-primary btn-block">
              Log In
            </button>
          </div>
        </div>
      </Form>
    </>
  );
};

export default AuthForm;
