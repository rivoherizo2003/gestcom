import { Form, useActionData } from "react-router-dom";
import Input from "../UI/Input";
import { EMAIL_ICON } from "../../constants/iconAwesome";

const ForgotPasswordForm = () => {
  const data = (useActionData() as ApiResponseType) ?? null;
  let classMsg = "login-box-msg";
  if (data !== null && !data.success) classMsg += " text-danger";
  const emailInput: InputType = {
    type: "email",
    Inputclasses: "form-control",
    placeholder: "Email",
    isRequired: true,
    defaultValue: "rivoherizo2003@hotmail.com",
    name: "email",
    icon: EMAIL_ICON,
  };

  return (
    <>
      <p className={classMsg}>
        {data === null &&
          "You forgot your password? Here you can easily retrieve a new password"}
        {data !== null && data.message}
      </p>
      {data === null && (
        <Form method="post">
          <div className="input-group mb-3">
            <Input {...emailInput} />
          </div>
          <div className="row">
            <div className="col-12">
              <button type="submit" className="btn btn-primary btn-block">
                Request new password
              </button>
            </div>
          </div>
        </Form>
      )}
    </>
  );
};

export default ForgotPasswordForm;
