import InputFormGroup from "../UI/InputFormGroup";
import TextareaFormGroup from "../UI/TextareaFormGroup";
import CustomRadio from "../UI/CustomRadio";
import { GENDER_MAN, GENDER_WOMAN } from "../../constants/person";
import { PHONE_ICON, USER_ICON } from "../../constants/iconAwesome";

const PersonFormFields = (personForm: PersonFormType) => {
  const isMrChecked = personForm.person?.gender === GENDER_MAN;
  const isMrsChecked = personForm.person?.gender === GENDER_WOMAN;


  return (
    <>
      <div className="form-group">
        <div className="row">
          <div className="col-2">
            <CustomRadio
              Inputclasses="custom-control-input"
              labelRadio="Man"
              value={GENDER_MAN}
              id="rd-men"
              name="gender"
              isChecked={isMrChecked}
              isRequired={false}
              type="radio"
            />
          </div>
          <div className="col-2">
            <CustomRadio
              Inputclasses="custom-control-input"
              labelRadio="Woman"
              value={GENDER_WOMAN}
              id="rd-women"
              name="gender"
              isChecked={isMrsChecked}
              isRequired={false}
              type="radio"
            />
          </div>
        </div>
      </div>

      <InputFormGroup
        labelTitle="Firstname:"
        type="text"
        Inputclasses="form-control"
        placeholder="Firstname"
        isRequired={true}
        name="firstname"
        id="text-firstname"
        defaultValue={personForm.person?.firstname}
        icon={USER_ICON}
        hasError={false}
      />

      <InputFormGroup
        labelTitle="Lastname:"
        type="text"
        Inputclasses="form-control"
        placeholder="Lastname"
        isRequired={true}
        name="lastname"
        id="text-lastname"
        defaultValue={personForm.person?.lastname}
        icon={USER_ICON}
        hasError={false}
      />

      <InputFormGroup
        labelTitle="Phone number:"
        type="text"
        Inputclasses="form-control"
        placeholder="Phone number"
        name="phone_number"
        id="text-phone-number"
        defaultValue={personForm.person?.phoneNumber}
        icon={PHONE_ICON}
        hasError={false}
        isRequired={false}
      />

      <TextareaFormGroup
        id="textarea-address"
        name="address"
        labelId="textarea-address"
        rows={3}
        placeholder="Address"
        labelTitle="Address:"
        defaultValue={personForm.person?.address}
      />
    </>
  );
};

export default PersonFormFields;
