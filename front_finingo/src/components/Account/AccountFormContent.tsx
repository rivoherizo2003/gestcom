import { useContext, useEffect, useState } from "react";
import { EMAIL_ICON, USER_ICON } from "../../constants/iconAwesome";
import InputFormGroup from "../UI/InputFormGroup";
import ReactSelect from "../UI/ReactSelect";
import UploadUserProfile from "../User/UploadUserProfile";
import useInput from "../../hooks/use-input";
import {
  getEmailErrorMessage,
  validateEmailFormat,
} from "../../helpers/email-helpers";
import { sendRequestTocheckEmailAvailability } from "../../services/Account/CheckEmailAvailability";
import { sendRequestTocheckUsernameAvailability } from "../../services/Account/CheckUsernameAvailability";
import { getRoleDescription } from "../../helpers/auth-helpers";
import RoleContext from "../../store/role-context";
import { getUsernameErrorMessage } from "../../helpers/account-helpers";

const isNotEmpty = (value: string | null) =>
  null !== value && value.trim() !== "";
const isEmail = (value: string | null) => validateEmailFormat(value);

const AccountFormContent = (props: AccountFormContentType) => {
  const [emailIsAvailable, setEmailIsAvailable] = useState(true);
  const [usernameIsAvailable, setUsernameIsAvailable] = useState(true);
  const [isPicProfileValid, setIsPicProfileValid] = useState(true);

  let defaultRolesOptions: SelectOptionType[] = [];
  let roles: SelectOptionType[] = [];

  const roleCtx = useContext(RoleContext);

  const {
    value: emailValue,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
  } = useInput(isEmail, {
    value: null !== props.account ? props.account.email : "",
    isTouched: false,
  });

  const {
    value: usernameValue,
    hasError: usernameHasError,
    valueChangeHandler: usernameChangeHandler,
  } = useInput(isNotEmpty, {
    value: null !== props.account ? props.account.username : "",
    isTouched: false,
  });

  if (props.account && props.account.roles) {
    props.account.roles.forEach((element) => {
      defaultRolesOptions.push({
        label: getRoleDescription(element),
        value: element,
      });
    });
  }
  if (roleCtx.roles.length > 0) {
    roleCtx.roles.forEach((element) => {
      roles.push({
        label: getRoleDescription(element),
        value: element,
      });
    });
  }

  const checkAvailabilityEmail = async () => {
    if (!emailHasError) {
      const ret: ApiResponseType = await sendRequestTocheckEmailAvailability(
        emailValue,
        null !== props.account && props.account.id ? props.account.id : null
      );
      
      setEmailIsAvailable(() => ret.success);
    }
  };

  const checkAvailabilityUsername = async () => {
    if (!usernameHasError) {
      const ret: ApiResponseType = await sendRequestTocheckUsernameAvailability(
        usernameValue,
        null !== props.account && props.account.id ? props.account.id : null
      );

      setUsernameIsAvailable(() => ret.success);
    }
  };

  const emailClasses: string =
    emailHasError || !emailIsAvailable
      ? "form-control is-invalid"
      : "form-control";

  const usernameClasses =
    usernameHasError || !usernameIsAvailable
      ? "form-control is-invalid"
      : "form-control";

  let messageEmailError: string | null = getEmailErrorMessage({
    emailIsInvalid: emailHasError,
    emailIsAvailable: emailIsAvailable,
  });

  let msgUsernameError: string | null = getUsernameErrorMessage({
    usernameIsAvailable: usernameIsAvailable,
    isUsernameEmpty: usernameHasError,
  });

  const checkPicProfileHasError = (isProfileValid: boolean) => {
    setIsPicProfileValid(() => isProfileValid);
  };

  const handleImgProfile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      props.handleSelectedImgProfile(event.target.files[0] ?? null);
    }
  };

  useEffect(() => {
    const formValidationStatus =
      emailIsAvailable &&
      usernameIsAvailable &&
      isPicProfileValid &&
      !emailHasError &&
      !usernameHasError;
    props.handleFormValidationStatus(formValidationStatus);
  }, [
    emailIsAvailable,
    usernameIsAvailable,
    isPicProfileValid,
    emailHasError,
    usernameHasError,
  ]);

  return (
    <>
      <UploadUserProfile
        userAvatar={props.userAvatar}
        profileFileName={""}
        maxFileSize={1024}
        handleImgProfile={handleImgProfile}
        checkPicProfileValid={checkPicProfileHasError}
      />
      <InputFormGroup
        labelTitle="Email:"
        type="email"
        Inputclasses={emailClasses}
        placeholder="Email"
        isRequired={true}
        name="email"
        id="text-email"
        value={emailValue}
        onChangeHandler={emailChangeHandler}
        onBlurHandler={checkAvailabilityEmail}
        icon={EMAIL_ICON}
        hasError={emailHasError || !emailIsAvailable ? true : false}
        errorMessage={messageEmailError}
      />
      <InputFormGroup
        labelTitle="Username:"
        type="text"
        Inputclasses={usernameClasses}
        placeholder="Username"
        isRequired={true}
        name="username"
        id="text-username"
        value={usernameValue}
        onChangeHandler={usernameChangeHandler}
        onBlurHandler={checkAvailabilityUsername}
        icon={USER_ICON}
        hasError={usernameHasError || !usernameIsAvailable ? true : false}
        errorMessage={msgUsernameError}
      />
      <ReactSelect
        id="select_role"
        nameInputHidden="roles"
        labelTitle="Role:"
        options={roles}
        defaultValue={defaultRolesOptions}
        isMulti={true}
        divFormGroup={{
          hasError: false,
          inputId: "select_role",
          errorMessage: null,
        }}
      />
    </>
  );
};

export default AccountFormContent;
