interface EmployeeProfileFormDataType {
  submitEvent: React.SyntheticEvent<HTMLFormElement>;
  imgProfile: File | null;
}

interface NewEmployeeInterface {
  position_title: string | "";
  comment: string | "";
  education: string | "";
  skills: string | "";
  account: NewAccountType | null;
}
