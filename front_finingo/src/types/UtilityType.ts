interface RequestConfigType {
  isAuthenticationNeeded: boolean;
  headers?: Headers;
  url: RequestInfo | URL;
  method?: string;
  body: string |FormData|null;
  useFormData: boolean;
}

interface MyToken {
  name: string;
  exp: string;
}

interface OAuth2LoaderType {
  token: string;
  usernameOrEmail: string;
  fullName?: string;
  profilePath?: string;
  isMeRemembered: boolean;
  expirationTime: number;
}

interface ApiResponseType{
  field: string;
  http_status: string;
  message: string;
  success: boolean;
}


interface DataListType{
  roles: string[];
}