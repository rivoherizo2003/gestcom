interface AuthFormtype {
  usernameOrEmail: string | null;
  isMeRemembered: boolean;
}

interface UploadUserProfileType {
  profileFileName: string;
  userAvatar: string;
  maxFileSize: number;
  handleImgProfile: (event: React.ChangeEvent<HTMLInputElement>) => void;
  checkPicProfileValid: (isProfileValid: boolean) => void | undefined;
}

interface AccountProfilePageType {
  account: Account;
  fullName: string;
  refreshUserProfileData: () => void;
}

interface AccountFormType {
  userAvatar: string;
  account: Account | null;
  handleClickBtnCancel?: () => void;
  handleAfterSubmit?: () => void;
}

interface ChangePwdPageType {
  accountId: number;
}

interface ProfileGeneralInfoPageType {
  employee: Employee;
  refreshUserProfileData: () => void;
}

interface AccountFormContentType {
  userAvatar: string;
  account: Account | null;
  handleFormValidationStatus: (status: boolean) => void;
  handleSelectedImgProfile: (file: File | null) => void;
}

interface AccountFormDataType {
  submitEvent: React.SyntheticEvent<HTMLFormElement>;
  imgProfile: File | null;
  account: Account | null;
}

interface UsernameErrorType{
  isUsernameEmpty: boolean;
  usernameIsAvailable: boolean;
}

interface NewAccountType{
  username:string;
  password?:string;
  email:string;
  roles:string[];
}
