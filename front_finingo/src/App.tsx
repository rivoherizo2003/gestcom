import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ProductsPage from "./pages/ProductsPage";
import RootLayout from "./pages/RootLayout";
import ErrorPage from "./pages/ErrorPage";
import ProductDetailPage from "./pages/ProductDetailPage";
import LoginPage from "./pages/LoginPage";
import RootAdminLayout from "./pages/admin/RootAdminLayout";
import DashboardPage from "./pages/admin/DashboardPage";
import { LoginAction } from "./pages/LoginAction";
import { checkTokenLoader, dashboardLoader } from "./helpers/auth-helpers";
import { LogoutLoader } from "./pages/LogoutLoader";
import { OAuth2Loader } from "./helpers/oauth2-helpers";
import ForgotPasswordPage from "./pages/ForgotPasswordPage";
import { ForgotPasswordAction } from "./pages/ForgotPasswordAction";
import { CheckTokenResetPwdLoader } from "./pages/CheckTokenResetPwdLoader";
import ResetPwdPage from "./pages/ResetPwdPage";
import { ResetPwdFormAction } from "./pages/ResetPwdFormAction";
import ProfileUserPage from "./pages/admin/UserProfile/ProfileUserPage";
import ListAccountsPage from "./pages/admin/Employee/ListEmployeesPage";
import RootAccountsLayout from "./pages/admin/Employee/RootAccountsLayout";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    id: "root",
    children: [
      {
        path: "/",
        element: <LoginPage />,
        action: LoginAction,
        loader: dashboardLoader,
      },
      { path: "products", element: <ProductsPage /> },
      { path: "products/:productId", element: <ProductDetailPage /> },
      { path: "logout", loader: LogoutLoader },
      {
        path: "forgot-password",
        element: <ForgotPasswordPage />,
        action: ForgotPasswordAction,
      },
      {
        path: "request/reset-password",
        element: <ResetPwdPage />,
        loader: CheckTokenResetPwdLoader,
        action: ResetPwdFormAction,
      },
    ],
  },
  {
    path: "admin",
    element: <RootAdminLayout />,
    loader: checkTokenLoader,
    children: [
      {
        path: "dashboard",
        element: <DashboardPage />,
      },
      {
        path: "profile/user",
        element: <ProfileUserPage />,
      },
      {
        path: "accounts",
        element: <RootAccountsLayout />,
        children: [
          {
            path: "list",
            element: <ListAccountsPage />,
          },
        ],
      },
    ],
  },
  {
    path: "oauth2/handler",
    loader: OAuth2Loader,
    errorElement: <ErrorPage />,
  },
]);

function App() {
  return <RouterProvider router={router}/>;
}

export default App;
