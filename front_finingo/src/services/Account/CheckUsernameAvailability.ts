export async function sendRequestTocheckUsernameAvailability(
  username: string,
  accountId: number | null
): Promise<ApiResponseType> {
  let urlParam: { username: string; accountId?: string } = {
    username: username,
  };
  if (null !== accountId) {
    urlParam.accountId = accountId.toString();
  }
  const response = await fetch(
    process.env["REACT_APP_API_URL"] +
      "/api/is-username-available?" +
      new URLSearchParams(urlParam).toString(),
    {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  const responseData = await response.json();

  if (response.status === 422 || response.status === 401) {
    throw new Error(responseData.message);
  }

  return responseData;
}
