export async function validateToken(token: string) {
  const response = await fetch(
    process.env["REACT_APP_API_URL"] + "/auth/validate/token",
    {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ token: token }),
    }
  );

  const responseData = await response.json();

  return responseData;
}
