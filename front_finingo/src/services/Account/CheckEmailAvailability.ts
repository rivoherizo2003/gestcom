export async function sendRequestTocheckEmailAvailability(
  email: string,
  accountId: number | null
):Promise<ApiResponseType> {
  let urlParam: { email: string; accountId?: string } = { email: email };
  if (null !== accountId) {
    urlParam.accountId = accountId.toString();
  }

  let urlCheckEmailAvailability =
    process.env["REACT_APP_API_URL"] +
    "/api/is-mail-available?" +
    new URLSearchParams(urlParam).toString();

  const response = await fetch(urlCheckEmailAvailability, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });

  const responseData = await response.json();

  if (response.status === 422 || response.status === 401) {
    throw new Error(responseData.message);
  }

  return responseData;
}
