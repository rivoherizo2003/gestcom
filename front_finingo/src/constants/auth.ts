
export const OAUTH2_REDIRECT_URI = process.env["REACT_APP_FRONT_URL"] +'/oauth2/handler';

export const GOOGLE_AUTH_URL = process.env["REACT_APP_API_URL"] + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;

export const FACEBOOK_AUTH_URL = process.env["REACT_APP_API_URL"] + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;

export const ERROR_USERNAME_NOT_AVAILABLE = "USERNAME_NOT_AVAILABLE";

export const ERROR_USERNAME_EMPTY = "USERNAME_FORMAT_EMPTY";