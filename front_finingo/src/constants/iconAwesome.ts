export const EMAIL_ICON = "fas fa-envelope";
export const USER_ICON = "fas fa-user";
export const PHONE_ICON = "fas fa-phone";
export const CAMERA_ICON = "fas fa-camera";
export const LOCK_ICON = "fas fa-lock";
