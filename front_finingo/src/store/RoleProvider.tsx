import { PropsWithChildren, useReducer } from "react";
import RoleContext from "./role-context";

interface RoleStateType {
  roles: string[];
}

interface RoleActionType {
  type: string;
  role?: string;
  roles?: string[];
}

const defaultRolesState: RoleStateType = {
  roles: [],
};

const roleReducer = (state: RoleStateType, action: RoleActionType) => {
  if (action.type === "create" && action.role) {
    return {
      roles: state.roles.concat(action.role),
    };
  }

  if(action.type === "init" && action.roles) {
    return {
      roles: action.roles,
    };
  }

  return defaultRolesState;
};

const RoleProvider = (props: PropsWithChildren) => {
  const [roleAccountState, dispatchRoleAccountAction] = useReducer(
    roleReducer,
    defaultRolesState
  );

  const addRole = (role: string) => {
    dispatchRoleAccountAction({ type: "create", role: role });
  };

  const addRoles = (roles: string[]) => {
    dispatchRoleAccountAction({
      type: "init",
      roles: roles,
    });
  };

  const roleContext = {
    roles: roleAccountState.roles,
    addRole: addRole,
    addRoles: addRoles,
  };


  return (
    <RoleContext.Provider value={roleContext}>
      {props.children}
    </RoleContext.Provider>
  );
};

export default RoleProvider;
