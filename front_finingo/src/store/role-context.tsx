import React from "react";

const roles: string[] = [];

const RoleContext = React.createContext({
  roles: roles,
  addRole: (_role: string) => {},
  addRoles: (_roles: string[]) => {},
});

export default RoleContext;
