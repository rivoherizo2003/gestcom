interface Person {
  gender: string;
  firstname: string | null;
  lastname: string;
  phoneNumber: string | null;
  address: string;
  id:number;
}
