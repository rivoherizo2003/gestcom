interface Account {
  id?:number;
  email: string;
  roles: string[];
  username: string;
  media_id?: number;
  user_code: string | null;
}
