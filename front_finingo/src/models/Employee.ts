interface Employee {
  positionTitle: string | null;
  skills: string | undefined;
  education: string | undefined;
  comment: string | undefined;
  person: Person;
}
