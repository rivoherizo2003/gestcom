interface UserProfile {
  account: Account;
  employee: Employee;
  handleOnChangeImgProfile?: any;
  handleEditProfileClick?: any;
}
