import { useCallback, useState } from "react";

interface ServerErrorType{
  status: number;
  error: string;
  message: string;
}

const useHttp = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null | ServerErrorType>(null);
  const sendRequest = useCallback(
    async (
      requestConfig: RequestConfigType,
      applyData: (arg0: any) => void
    ) => {
      setIsLoading(true);
      setError(null);
      if (requestConfig.isAuthenticationNeeded) {
        if (!requestConfig.headers){
          requestConfig.headers = new Headers();
        }
        requestConfig.headers.append(
          "Authorization",
          "Bearer " + localStorage.getItem("token")
        );
      }

      try {
        const response = await fetch(requestConfig.url, {
          method: requestConfig.method ? requestConfig.method : "GET",
          headers: requestConfig.headers ? requestConfig.headers : {},
          body:requestConfig.body
        });

        if (response.status === 422 || response.status === 401) {
          return response;
        }

        const contentType = response.headers.get("Content-Type");
        let data;
        if(contentType && contentType.includes("application/json")){
          data = await response.json();
        } else {
          data = await response.blob();
        }

        applyData(data);
        setIsLoading(() => false);

        return data;
      } catch (error) {
        setError(() => "An error occured");
      }
    },
    []
  );

  return {
    isLoading,
    error,
    sendRequest,
  };
};

export default useHttp;
