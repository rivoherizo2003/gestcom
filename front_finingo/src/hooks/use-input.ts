import { useReducer } from "react";

interface InputState {
  value: string;
  isTouched: boolean;
}

type ValidateFunction = (value: any) => boolean;

const INPUT = "INPUT";
const BLUR = "BLUR";
const RESET = "RESET";

const defaultInputState: InputState = {
  value: "",
  isTouched: false,
};

const inputStateReducer = (
  state: InputState,
  action: { type: string; value: any }
) => {
  if (action.type === INPUT) {
    return { value: action.value, isTouched: true };
  }

  if (action.type === BLUR) {
    // console.log(action.value + " vs "+state.value);
    return { isTouched: true, value: state.value };
  }

  if (action.type === RESET) {
    return { isTouched: false, value: "" };
  }

  return state;
};

const useInput = (
  validateValue: ValidateFunction,
  initialInputState?: InputState
) => {
  let valueIsValid = true;
  let hasError = false;
  const [inputState, dispatch] = useReducer(
    inputStateReducer,
    initialInputState ? initialInputState : defaultInputState
  );
  valueIsValid = validateValue(inputState.value);
  hasError = !valueIsValid && inputState.isTouched;

  const valueChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({ type: INPUT, value: event.target.value });
  };

  const inputBlurHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({
      type: BLUR,
      value: event.target.value,
    });
  };

  const reset = () => {
    dispatch({
      type: RESET,
      value: undefined,
    });
  };

  return {
    value: inputState.value,
    isValid: valueIsValid,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
  };
};

export default useInput;
