export function alterateFormData(employeeFormData: EmployeeProfileFormDataType) {
  const formData = new FormData();
  if (employeeFormData.imgProfile) {
    formData.append("profile_img", employeeFormData.imgProfile);
  }

  let formDataWithNoFile = new FormData(
    employeeFormData.submitEvent.currentTarget
  );
  const newEmployeeData = JSON.parse(
    JSON.stringify(Object.fromEntries(formDataWithNoFile))
  );
  const roles:string[] = Array.from(
    document.querySelectorAll<HTMLInputElement>('input[name="roles"]')
  ).map((input: HTMLInputElement) => input.value);
  
  const newEmployeeRequest:NewEmployeeInterface = {
    position_title: newEmployeeData['position_title'],
    comment: newEmployeeData['comment'],
    education: newEmployeeData['education'],
    skills: newEmployeeData['skills'],
    account: {
      username: newEmployeeData['username'],
      email: newEmployeeData['email'],
      roles: roles,
    }
  };
  
  formData.append("new_employee_request", JSON.stringify(newEmployeeRequest));

  return formData;
}
