const toBase64 = (file: File) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
  });

export async function convertFileToBase64(file: File): Promise<string | Blob> {
  try {
    const result = await toBase64(file);

    return result as string | Blob;
  } catch (error) {
    console.trace("Error from toBase64");

    return "Error from toBase64";
  }
}

export function previewFile(file: File, img: HTMLImageElement ){
  const reader: FileReader = new FileReader();

  reader.addEventListener("load", () =>{
    img.src = typeof reader.result === "string" ? reader.result :"";
  }, false);

  if(file){
    reader.readAsDataURL(file);
  }
}
