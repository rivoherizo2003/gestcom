import { ERROR_USERNAME_EMPTY, ERROR_USERNAME_NOT_AVAILABLE } from "../constants/auth";

export function alterateFormData(accountFormData: AccountFormDataType) {
  const formData = new FormData();
  if (accountFormData.imgProfile) {
    formData.append("profile_img", accountFormData.imgProfile);
  }

  let formDataWithNoFile = new FormData(
    accountFormData.submitEvent.currentTarget
  );
  let jsonData = JSON.parse(
    JSON.stringify(Object.fromEntries(formDataWithNoFile))
  );
  jsonData.roles = Array.from(
    document.querySelectorAll<HTMLInputElement>('input[name="roles"]')
  ).map((input: HTMLInputElement) => input.value);
  jsonData["id"] = accountFormData.account?.id;
  formData.append("profile_request", JSON.stringify(jsonData));

  return formData;
}

export function getUsernameErrorMessage(error: UsernameErrorType) {
  if(error.isUsernameEmpty) return getTextErrorMessageByType(ERROR_USERNAME_EMPTY);

  if(!error.usernameIsAvailable) return getTextErrorMessageByType(ERROR_USERNAME_NOT_AVAILABLE);

  return null;

}

function getTextErrorMessageByType(typeError: string): string {
  let message = "";

    switch (typeError) {
        case ERROR_USERNAME_NOT_AVAILABLE:
          message = "This username is already in use";
          break;
        case ERROR_USERNAME_EMPTY:
          message = "Username could not be empty";
          break;
    
        default:
          message = "Username error not recognized";
          break;
      }
    
      return message;
}
