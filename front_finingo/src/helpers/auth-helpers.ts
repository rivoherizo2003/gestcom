import { redirect } from "react-router-dom";

export function getTokenDuration() {
  let duration = 0;
  const storedExpiration = localStorage.getItem("expiration");
  if (storedExpiration !== null) {
    const now = new Date();
    const expirDate = new Date(parseInt(storedExpiration) * 1000);
    duration = expirDate.getTime() - now.getTime();
  }

  return duration;
}

export function getAuthToken() {
  const token = localStorage.getItem("token");
  if (!token) {
    return null;
  }

  const tokenDuration = getTokenDuration();

  if (tokenDuration < 0) return null;

  return token;
}

export function tokenLoader() {
  const token = getAuthToken();

  return token;
}

export function checkTokenLoader() {
  const token = getAuthToken();

  if (!token || token === undefined) {
    return redirect("/");
  }

  return null;
}

export function dashboardLoader() {
  const token = getAuthToken();

  if (token && token !== undefined) {
    return redirect("/admin/dashboard");
  }

  return null;
}

export function isTapedPasswordValid(password: string) {
  const reg = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])");

  if (password.length > 6 && reg.test(password)) return true;

  return false;
}

export function feedLocalStorage(authData: OAuth2LoaderType) {
  localStorage.setItem("token", authData.token);
  localStorage.setItem("username", authData.usernameOrEmail);
  localStorage.setItem("fullName", authData.fullName ?? "");
  localStorage.setItem("profilePath", authData.profilePath ?? "");
  localStorage.setItem("isMeRemembered", authData.isMeRemembered ? "1" : "0");
  localStorage.setItem("expiration", authData.expirationTime.toString());
}

export function getRoleDescription(role: string) {
  switch (role) {
    case 'ROLE_ADMIN': return 'Administrator';
    case 'ROLE_USER': return 'User';
    default: return 'Unknown role';
  }
}
