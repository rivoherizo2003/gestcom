import jwtDecode from "jwt-decode";
import { LoaderFunctionArgs, redirect } from "react-router-dom";
import { feedLocalStorage } from "./auth-helpers";

export async function OAuth2Loader(loader: LoaderFunctionArgs) {
  const urlData = loader.request.url.split("?");
  const data: OAuth2LoaderType = extractDataFromUrl(urlData);
  feedLocalStorage(data);

  return redirect("/admin/dashboard");
}

const extractDataFromUrl = (urlData: string[]): OAuth2LoaderType => {
  let data: OAuth2LoaderType = {
    token: "",
    usernameOrEmail: "",
    isMeRemembered: false,
    expirationTime: 0,
  };
  if (urlData.length > 0) {
    const paramsData = urlData[1]?.split("&");
    if (paramsData !== undefined && paramsData.length > 1) {
      const tokenData = paramsData[0]?.split("=");
      const usernameOrEmailData = paramsData[1]?.split("=");

      data.token = tokenData && undefined !== tokenData[1] ? tokenData[1] : "";
      data.usernameOrEmail =
        usernameOrEmailData && usernameOrEmailData[1]
          ? usernameOrEmailData[1]
          : "";

      const tokenDataDecoded = jwtDecode<MyToken>(data.token);

      data.expirationTime = parseInt(tokenDataDecoded.exp);
    }
  }

  return data;
};
