export const ERROR_MAIL_NOT_AVAILABLE = "EMAIL_NOT_AVAILABLE";
export const ERROR_MAIL_FORMAT_INVALID = "EMAIL_FORMAT_NOT_VALID";

export function validateEmailFormat(email: string | null): boolean {
  return null !== email && email.includes("@");
}

interface EmailErrorType{
    emailIsInvalid: boolean;
    emailIsAvailable: boolean;
}

export function getEmailErrorMessage(error: EmailErrorType) {
  if(error.emailIsInvalid) return getTextErrorMessageByType(ERROR_MAIL_FORMAT_INVALID);

  if(!error.emailIsAvailable) return getTextErrorMessageByType(ERROR_MAIL_NOT_AVAILABLE);

  return null;

}

function getTextErrorMessageByType(typeError: string): string {
  let message = "";

    switch (typeError) {
        case ERROR_MAIL_NOT_AVAILABLE:
          message = "This mail is already in use";
          break;
        case ERROR_MAIL_FORMAT_INVALID:
          message = "You have typed wrong email format";
          break;
    
        default:
          message = "Email error not recognized";
          break;
      }
    
      return message;
}
