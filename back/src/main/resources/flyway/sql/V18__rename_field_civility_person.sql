ALTER TABLE gc_person RENAME COLUMN civility TO gender;
ALTER TABLE gc_person ALTER COLUMN gender TYPE CHAR(2);