-- create some fields for table gc_product
ALTER TABLE gc_product ADD COLUMN disc VARCHAR(10);
ALTER TABLE gc_product ADD COLUMN drug_code VARCHAR(20);
ALTER TABLE gc_product ADD COLUMN notice TEXT;
ALTER TABLE gc_product ADD COLUMN DATE TIMESTAMPTZ;
ALTER TABLE gc_product ADD COLUMN CODE VARCHAR(20);
ALTER TABLE gc_product ADD COLUMN quantity FLOAT;