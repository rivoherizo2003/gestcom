--Change type price in product table
--Add new field createdby and updatedby
ALTER TABLE gc_product
ALTER COLUMN price TYPE NUMERIC(12,2);
ALTER TABLE gc_product
ADD created_by bigint NULL;
ALTER TABLE gc_product
ADD updated_by bigint NULL;


