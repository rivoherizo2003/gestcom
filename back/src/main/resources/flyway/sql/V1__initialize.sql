CREATE TABLE "public"."gc_category" (
    id SERIAL PRIMARY KEY,
    category_name varchar(255) NOT NULL,
    description varchar(255)
);

CREATE TABLE "public"."gc_product" (
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,
    price money,
    category_id integer REFERENCES gc_category(id),
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ
);

CREATE TABLE "public"."gc_account" (
	id SERIAL PRIMARY KEY,
	username varchar(20) NOT NULL UNIQUE,
	email varchar(50) NOT NULL UNIQUE,
	password varchar(255) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ
);

CREATE TABLE "public"."gc_roles" (
	id SERIAL PRIMARY KEY,
	name_role varchar(50) NOT NULL
); 

CREATE TABLE "public"."gc_account_roles" (
	account_id integer REFERENCES gc_account (id),
	role_id integer REFERENCES gc_roles(id)
);