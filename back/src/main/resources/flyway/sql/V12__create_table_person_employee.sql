CREATE TABLE "public"."gc_person"
(
    id          SERIAL PRIMARY KEY,
    first_name        varchar(50),
    last_name        varchar(150),
    civility varchar(10),
    account_id integer REFERENCES gc_account (id),
    created_at  TIMESTAMPTZ  NOT NULL,
    updated_at  TIMESTAMPTZ,
    comment TEXT,
    address TEXT
);