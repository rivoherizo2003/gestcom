-- password admin123456
INSERT INTO gc_account (id, username, email, password, created_at, updated_at, media_id, user_code) VALUES (1, 'admin', 'admin@finengo.com', '$2a$10$18Mu1O70Jz0MmDnLzCUEMeoV61HnBWc9vQQZfA7/i4dvxxzYBWK3e', '2023-05-02 14:42:01.832621 +00:00', '2023-05-02 14:42:01.832621 +00:00', null, '2023-144201');

INSERT INTO gc_account_roles (account_id, role_id) VALUES (1, 2);