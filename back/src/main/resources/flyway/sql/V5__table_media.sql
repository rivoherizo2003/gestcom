--create table gc_media
CREATE TABLE "public"."gc_media" (
   id SERIAL PRIMARY KEY,
   file_name varchar(100) NOT NULL,
   real_file_name varchar(100) NOT NULL,
   extension char(8) NOT NULL,
   file_path varchar(200) NOT NULL,
   created_at TIMESTAMPTZ NOT NULL,
   updated_at TIMESTAMPTZ
);
--create the field to use as secondary key in gc_account
ALTER TABLE gc_account ADD COLUMN media_id INTEGER;

--create secondary key for gc_media link with table account
ALTER TABLE gc_account
ADD CONSTRAINT fk_media_id
FOREIGN KEY (media_id)
REFERENCES gc_media(id)
ON DELETE CASCADE;