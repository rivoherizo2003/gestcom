package com.ozireh.finengo.services.account;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.enumerator.ProviderEnum;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.dao.RoleDao;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProcessOauth2Service {
    private final AccountDao getAccountService;
    private final RoleDao getRoleService;
    private final AccountRepository accountRepository;

    public ProcessOauth2Service(AccountDao getAccountService, RoleDao getRoleService, AccountRepository accountRepository){
        this.getAccountService = getAccountService;
        this.getRoleService = getRoleService;
        this.accountRepository = accountRepository;
    }

    public Account execute(String usernameOrEmail, ProviderEnum provider){
        Account ac;
        Boolean bIsAccountExists = this.getAccountService.existByEmail(usernameOrEmail);
        if (!bIsAccountExists){
            String userCode = new SimpleDateFormat("yyyy-HHmmss").format(new Date());
            List<String> roles = new ArrayList<>();
            roles.add(RoleEnum.ROLE_USER.toString());
            Account account = Account.getBuilder()
                    .withEmail(usernameOrEmail)
                    .withUserCode(userCode)
                    .withProvider(provider)
                    .withPassword(RandomStringUtils.randomAlphabetic(50))
                    .withRoles(getRoleService.findNames(roles))
                    .build();

             return this.accountRepository.save(account);
        }

        return getAccountService.findByEmail(usernameOrEmail).orElse(null);
    }
}
