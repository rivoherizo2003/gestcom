package com.ozireh.finengo.services.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.UserProfileResponse;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.services.employee.CreateEmployeeResponseService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreateUserProfileResponseService {
    private final AccountRepository accountRepository;
    private final CreateEmployeeResponseService createEmployeeResponseService;
    private final CreateAccountResponseService createAccountResponseService;

    public CreateUserProfileResponseService(AccountRepository accountRepository, CreateEmployeeResponseService buildEmployeeResponseByAccount, CreateAccountResponseService buildAccountResponseByAccount) {
        this.accountRepository = accountRepository;
        this.createEmployeeResponseService = buildEmployeeResponseByAccount;
        this.createAccountResponseService = buildAccountResponseByAccount;
    }

    public UserProfileResponse createFromAccountId(Long id) {
        Optional<Account> accountOptional = accountRepository.findById(id);
        UserProfileResponse userProfileResponse = new UserProfileResponse();
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            userProfileResponse.account = createAccountResponseService.create(account);
            userProfileResponse.employee = createEmployeeResponseService.createFromAccount(account);

            return userProfileResponse;
        }
        // TODO to refactor because its a very bad idea to return null. Investigate around how to use object null pattern
        return null;
    }
}
