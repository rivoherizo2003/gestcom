package com.ozireh.finengo.services.account;

import com.ozireh.finengo.dao.RoleDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.payloads.requests.AccountRequest;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.services.media.SaveMediaProfileAccountService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class CreateAccountService {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleDao roleDao;

    public CreateAccountService(PasswordEncoder passwordEncoder, RoleDao roleDao, SaveMediaProfileAccountService handleMedia, AccountRepository accountRepository) {
        this.passwordEncoder = passwordEncoder;
        this.roleDao = roleDao;
        this.accountRepository = accountRepository;
    }

    public Account create(AccountRequest accountRequest){
        Set<Role> roles = roleDao.findNames(Arrays.stream(accountRequest.getRoles()).collect(Collectors.toList()));
        String userCode = new SimpleDateFormat("yyyy-HHmmss").format(new Date());
        Account account = Account.getBuilder()
                .withPassword(passwordEncoder.encode(accountRequest.getPassword()))
                .withRoles(roles)
                .withEmail(accountRequest.getEmail())
                .withUserCode(userCode)
                .build();

        return accountRepository.save(account);
    }
}
