package com.ozireh.finengo.services.drug;

import com.ozireh.finengo.model.Drug;
import com.ozireh.finengo.repository.DrugRepository;
import org.springframework.stereotype.Service;

@Service
public class GetByIdDrugServiceImpl implements GetByIdDrugService {
    final
    DrugRepository drugRepository;

    public GetByIdDrugServiceImpl(DrugRepository drugRepository) {
        this.drugRepository = drugRepository;
    }

    public Drug execute(Long id) {
        return drugRepository.findById(id).orElse(null);
    }
}
