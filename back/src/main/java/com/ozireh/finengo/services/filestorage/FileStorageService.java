package com.ozireh.finengo.services.filestorage;

import com.ozireh.finengo.model.Media;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorageService {
    void uploadMultipart(MultipartFile file, String folderPath) throws FileUploadException, IOException;

    public void uploadMediaBase64(String fileBase64, Media media) throws FileUploadException;

    public String generateUniqueFileName(String targetFolderPath, String extension);
}
