package com.ozireh.finengo.services.account;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.requests.AccountRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ValidateSignUpRequestService {
    private final AccountDao getAccountService;

    public ValidateSignUpRequestService(AccountDao getAccountService){

        this.getAccountService = getAccountService;
    }

    public ApiResponse validate(AccountRequest signUpRequest){
        if (getAccountService.existByUsername(signUpRequest.getUsername())) {
            return new ApiResponse(false,
                    "Username is already in user","username", HttpStatus.BAD_REQUEST);
        }

        if (getAccountService.existByEmail(signUpRequest.getEmail())) {
            return new ApiResponse(false,
                    "Email is already in use","email", HttpStatus.BAD_REQUEST);
        }

        return new ApiResponse(true, "User registered successfully", "username", HttpStatus.CREATED);
    }
}
