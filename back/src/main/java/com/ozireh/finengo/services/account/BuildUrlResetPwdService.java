package com.ozireh.finengo.services.account;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.ozireh.finengo.config.AppProperties;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.security.JwtTokenProvider;

@Service
public class BuildUrlResetPwdService {
    private final JwtTokenProvider jwtTokenProvider;
    private final AppProperties appProperties;
    private final AuthenticationManager authenticationManager;

    public BuildUrlResetPwdService(JwtTokenProvider jwtTokenProvider, AppProperties appProperties,AuthenticationManager authenticationManager){

        this.jwtTokenProvider = jwtTokenProvider;
        this.appProperties = appProperties;
        this.authenticationManager = authenticationManager;
    }

    protected String getUrlResetPwd(Account account){
        String password = RandomStringUtils.randomAlphabetic(50);
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(account.getEmail(), password));
        String token = jwtTokenProvider
                .generateToken(authentication, 3600000);

        return UriComponentsBuilder.fromUriString(appProperties.getUrlFrontResetPwd())
                .queryParam("token", token)
                .build().toUriString();
    }
}
