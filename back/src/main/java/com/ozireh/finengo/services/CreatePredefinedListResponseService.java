package com.ozireh.finengo.services;

import org.springframework.stereotype.Service;

import com.ozireh.finengo.builder.predefinedlist.PredefinedListResponseBuilder;
import com.ozireh.finengo.dao.RoleDao;
import com.ozireh.finengo.payloads.responses.PredefinedListResponse;

@Service
public class CreatePredefinedListResponseService {
    private final PredefinedListResponseBuilder predefinedListResponseBuilder;
    private final RoleDao roleDao;

    public CreatePredefinedListResponseService(RoleDao roleDao, PredefinedListResponseBuilder predefinedListResponseBuilder){
        this.roleDao = roleDao;

        this.predefinedListResponseBuilder = predefinedListResponseBuilder;
    }

    public PredefinedListResponse create(){
        return predefinedListResponseBuilder
                .withRoles(this.roleDao.findAllRoleNames())
                .build();
    }
}
