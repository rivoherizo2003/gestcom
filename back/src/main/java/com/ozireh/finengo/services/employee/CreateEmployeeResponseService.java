package com.ozireh.finengo.services.employee;

import com.ozireh.finengo.builder.employee.EmployeeResponseBuilder;
import com.ozireh.finengo.builder.person.PersonResponseBuilder;
import com.ozireh.finengo.dao.EmployeeDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.payloads.responses.EmployeeResponse;
import com.ozireh.finengo.payloads.responses.PersonResponse;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreateEmployeeResponseService {

    private final EmployeeDao employeeDao;
    private final EmployeeResponseBuilder employeeResponseBuilder;
    private final PersonResponseBuilder personResponseBuilder;

    public CreateEmployeeResponseService(EmployeeDao employeeDao, EmployeeResponseBuilder employeeResponseBuilder, PersonResponseBuilder personResponseBuilder){
        this.employeeDao = employeeDao;
        this.employeeResponseBuilder = employeeResponseBuilder;
        this.personResponseBuilder = personResponseBuilder;
    }

    public EmployeeResponse createFromAccount(Account account){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        Optional<Employee> employeeOptional = employeeDao.findEmployeeByAccount(account);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            PersonResponse personResponse = personResponseBuilder
                    .withFirstname(employee.getFirstName())
                    .withLastname(employee.getLastName())
                    .withGender(employee.getGender().getValue())
                    .withPhoneNumber(employee.getPhoneNumber())
                    .withAddress(employee.getAddress())
                    .build();

            employeeResponse = employeeResponseBuilder
                    .withPositionTitle(employee.getPositionTitle())
                    .withComment(employee.getComment())
                    .withEducation(employee.getEducation())
                    .withSkills(employee.getSkills())
                    .withPerson(personResponse)
                    .build();
        }
        
        return employeeResponse;
    }

    public EmployeeResponse createFromEmployee(Employee employee){
        PersonResponse personResponse = personResponseBuilder
                .withFirstname(employee.getFirstName())
                .withLastname(employee.getLastName())
                .withGender(employee.getGender().getValue())
                .withPhoneNumber(employee.getPhoneNumber())
                .withAddress(employee.getAddress())
                .build();

        return employeeResponseBuilder
                .withPositionTitle(employee.getPositionTitle())
                .withComment(employee.getComment())
                .withEducation(employee.getEducation())
                .withSkills(employee.getSkills())
                .withPerson(personResponse)
                .build();
    }
}
