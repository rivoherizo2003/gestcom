package com.ozireh.finengo.services.drug;

import com.ozireh.finengo.model.Drug;

public interface GetByIdDrugService {
    Drug execute(Long id);
}
