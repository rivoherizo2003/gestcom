package com.ozireh.finengo.services.filestorage;

import com.ozireh.finengo.model.Media;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    public void uploadMultipart(MultipartFile file, String filePath) throws FileUploadException {
        try {
            Path path = FileSystems.getDefault().getPath(filePath);
            System.out.println(path.getParent().toString() + " <==> " + filePath);
            if (!Files.exists(path.getParent())) {
                System.out.println("Creating directory " + path.getParent());
                Files.createDirectories(path.getParent());
                System.out.println("directory created" + path.getParent());
            }
            file.transferTo(new File(filePath));
        } catch (Exception e) {
            throw new FileUploadException("Could not upload the file. Error: " + e.getMessage());
        }
    }

    @Override
    public void uploadMediaBase64(String fileBase64, Media media) throws FileUploadException {
        String[] dataBase64 = fileBase64.split(",");
        byte[] decodedData = Base64.getDecoder()
                .decode(dataBase64[1]);
        try {
            OutputStream stream = new FileOutputStream(media.getFilePath());
            stream.write(decodedData);
            stream.close();
        } catch (Exception e) {
            throw new FileUploadException("Could not upload the file. Error: " + e.getMessage());
        }
    }

    private String generateFileName(String extension) {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

        return timeStamp + "." + extension;
    }

    public String generateUniqueFileName(String targetFolderPath, String extension) {
        File f;
        String fileName, filePath;
        do {
            fileName = generateFileName(extension);
            filePath = targetFolderPath + "/" + fileName;
            f = new File(filePath);
        } while (f.exists());

        return fileName;
    }
}
