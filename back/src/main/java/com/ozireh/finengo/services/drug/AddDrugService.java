package com.ozireh.finengo.services.drug;

import com.ozireh.finengo.model.Drug;
import com.ozireh.finengo.payloads.requests.DrugRequest;

public interface AddDrugService {
    Drug execute(DrugRequest drugRequest);
}
