package com.ozireh.finengo.services.account;

import java.util.HashSet;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ozireh.finengo.builder.account.AccountResponseBuilder;
import com.ozireh.finengo.enumerator.ProviderEnum;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.payloads.responses.AccountInfoResponse;

@Service
public class CreateAccountResponseService {

    private final AccountResponseBuilder accountResponseBuilder;

    public CreateAccountResponseService(AccountResponseBuilder accountResponseBuilder) {
        this.accountResponseBuilder = accountResponseBuilder;
    }

    public AccountInfoResponse create(Account account) {
        accountResponseBuilder.withIdentifier(account.getId())
                .withMail(account.getEmail())
                .withUsername(account.getUsername())
                .withUserCode(account.getUserCode())
                .withUserRoles(account.getRoles().stream().map((Role::getName)).collect(Collectors.toCollection(HashSet::new)))
                .withProvider(null != account.getProvider() ? account.getProvider().name() : ProviderEnum.LOCAL.name());

        if (null != account.getMedia()) {
            accountResponseBuilder.withMediaIdentifier(account.getMedia().getId());
        }

        return accountResponseBuilder.build();
    }
}
