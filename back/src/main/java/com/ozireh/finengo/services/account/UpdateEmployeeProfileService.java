package com.ozireh.finengo.services.account;

import com.ozireh.finengo.dao.EmployeeDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.payloads.requests.EmployeeRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdateEmployeeProfileService {
    private final EmployeeDao employeeDao;

    UpdateEmployeeProfileService(EmployeeDao employeeDao){
        this.employeeDao = employeeDao;
    }
    public Employee update(Account loggedAccount, EmployeeRequest employeeRequest){
        /*
         * Update the employee
         */
        Optional<Employee> optionalEmployee = employeeDao.findEmployeeByAccount(loggedAccount);
        if (optionalEmployee.isPresent()) {
            Employee employee = Employee.getBuilder()
                    .withFirstname(employeeRequest.getFirstname())
                    .withLastname(employeeRequest.getLastname())
                    .withGender(employeeRequest.getGender())
                    .withComment(employeeRequest.getComment())
                    .withSkills(employeeRequest.getSkills())
                    .withEducation(employeeRequest.getEducation())
                    .withPositionTitle(employeeRequest.getPosition_title())
                    .withAddress(employeeRequest.getAddress())
                    .withPhoneNumber(employeeRequest.getPhone_number())
                    .build(optionalEmployee.get());
            System.err.println(employee.getGender());
            employeeDao.save(employee);

            return employee;
        }

        return null;
    }
}
