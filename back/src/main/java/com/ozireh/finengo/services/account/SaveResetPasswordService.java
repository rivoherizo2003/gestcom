package com.ozireh.finengo.services.account;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.requests.ResetPasswordRequest;
import com.ozireh.finengo.repository.AccountRepository;

@Service
public class SaveResetPasswordService {
    private final PasswordEncoder passwordEncoder;
    private final AccountRepository accountRepository;

    public SaveResetPasswordService(PasswordEncoder passwordEncoder, AccountRepository accountRepository) {
        this.passwordEncoder = passwordEncoder;
        this.accountRepository = accountRepository;
    }

    public void resetPassword(Account account, ResetPasswordRequest resetPasswordRequest)
    {
        account = Account.getBuilder()
                .withPassword(passwordEncoder.encode(resetPasswordRequest.getPassword()))
                .build(account);
        accountRepository.save(account);
    }
}
