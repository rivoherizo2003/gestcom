package com.ozireh.finengo.services.media;

import java.util.Objects;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.dao.MediaDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Media;
import com.ozireh.finengo.services.filestorage.FileStorageService;

@Service
public class SaveMediaProfileAccountService {
    @Value("${folder.upload.path}")
    private String rootUploadFolder;

    private final MediaDao mediaDao;
    private final AccountDao accountDao;
    private final FileStorageService fileStorageService;

    public SaveMediaProfileAccountService(MediaDao mediaDao, AccountDao accountDao, FileStorageService fileStorageService){
        this.mediaDao = mediaDao;
        this.accountDao = accountDao;
        this.fileStorageService = fileStorageService;
    }

    public Media save(MultipartFile file, Account account) throws Exception {
        String realFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        StringBuilder sbFolder = new StringBuilder();
        sbFolder.append(rootUploadFolder);
        sbFolder.append("/");
        sbFolder.append(account.getUserCode());

        String fileName = null == account.getMedia()? fileStorageService.generateUniqueFileName(sbFolder.toString(), extension):account.getMedia().getFileName();
        StringBuilder sbFilePath = new StringBuilder();
        sbFilePath.append(sbFolder);
        sbFilePath.append("/");
        sbFilePath.append(fileName);

        Media media = Media.getBuilder()
                .withFileName(fileName)
                .withRealFileName(realFileName)
                .withExtension(extension)
                .withFilePath(sbFilePath.toString())
                .build(null != account.getMedia()?account.getMedia():new Media());

        media = mediaDao.saveMedia(media);

        accountDao.save(Account.getBuilder().withMedia(media).build(account));

        fileStorageService.uploadMultipart(file, sbFilePath.toString());

        return media;
    }

