package com.ozireh.finengo.services.employee;

import com.ozireh.finengo.dao.EmployeeDao;
import com.ozireh.finengo.exception.EmployeeNotCreatedException;
import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.payloads.requests.NewEmployeeRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateEmployeeService {
    private final EmployeeDao employeeDao;

    public CreateEmployeeService(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    public Employee create(NewEmployeeRequest newEmployeeRequest) throws EmployeeNotCreatedException {
        Employee employee = Employee.getBuilder()
                .withGender(newEmployeeRequest.getGender())
                .withFirstname(newEmployeeRequest.getFirstname())
                .withLastname(newEmployeeRequest.getLastname())
                .withAddress(newEmployeeRequest.getAddress())
                .withPhoneNumber(newEmployeeRequest.getPhone_number())
                .withComment(newEmployeeRequest.getComment())
                .withEducation(newEmployeeRequest.getEducation())
                .withPositionTitle(newEmployeeRequest.getPosition_title())
                .withSkills(newEmployeeRequest.getSkills())
                .build();

        return employeeDao.save(employee);
    }
}
