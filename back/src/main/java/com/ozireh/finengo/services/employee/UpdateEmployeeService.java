package com.ozireh.finengo.services.employee;

import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.payloads.requests.EmployeeRequest;
import com.ozireh.finengo.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

@Service
public class UpdateEmployeeService {
    private final EmployeeRepository employeeRepository;

    public UpdateEmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    /**
     *
     * @param employeeRequest employee data's
     * @param employee employee occurrence
     * @return employee
     */
    public Employee update(EmployeeRequest employeeRequest, Employee employee){
        Employee employeeUpdated = Employee.getBuilder()
                .withFirstname(employeeRequest.getFirstname())
                .withLastname(employeeRequest.getLastname())
                .withAddress(employeeRequest.getAddress())
                .withPhoneNumber(employeeRequest.getPhone_number())
                .withGender(employeeRequest.getGender())
                .withComment(employeeRequest.getComment())
                .withSkills(employeeRequest.getSkills())
                .withEducation(employeeRequest.getEducation())
                .withPositionTitle(employeeRequest.getPosition_title())
                .build(employee);

        return employeeRepository.save(employeeUpdated);
    }
}
