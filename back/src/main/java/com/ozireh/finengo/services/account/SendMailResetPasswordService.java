package com.ozireh.finengo.services.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Mail;
import com.ozireh.finengo.services.mailing.SendEmailService;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

@Service
public class SendMailResetPasswordService {

    private final SendEmailService sendEmailService;
    private final BuildUrlResetPwdService buildUrlResetPwd;

    public SendMailResetPasswordService(SendEmailService sendEmailService, BuildUrlResetPwdService buildUrlResetPwd){
        this.sendEmailService = sendEmailService;
        this.buildUrlResetPwd = buildUrlResetPwd;
    }

    public void sendMailResponse(Account account) throws MessagingException {
        Mail mail = new Mail();
        mail.setFrom("admin@finingo.com");
        mail.setMailTo(account.getEmail());
        mail.setSubject("Reset password");
        mail.setTemplateName("new-pwd-request");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("location", "Madagascar");
        model.put("sign", "Finingo Team");
        mail.setProps(model);
        String urlResetPwd = buildUrlResetPwd.getUrlResetPwd(account);
        model.put("urlResetPwd", urlResetPwd);

        sendEmailService.sendEmail(mail);
    }
}
