package com.ozireh.finengo.services.account;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.dao.RoleDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.payloads.requests.ProfileRequest;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UpdateAccountService {
    private final RoleDao roleDao;
    private final AccountDao accountDao;

    UpdateAccountService(RoleDao roleDao, AccountDao accountDao) {
        this.roleDao = roleDao;
        this.accountDao = accountDao;
    }

    /**
     * @param profileRequest profile data request
     * @param loggedAccount               account logged
     * @return Account
     */
    public Account update(ProfileRequest profileRequest, Account loggedAccount) {
        Set<Role> roles = roleDao.findNames(Arrays.stream(profileRequest.getRoles()).collect(Collectors.toList()));
        Account account = Account.getBuilder()
                .withEmail(profileRequest.getEmail())
                .withUsername(profileRequest.getUsername())
                .withRoles(roles)
                .build(loggedAccount);

        return accountDao.save(account);
    }
}
