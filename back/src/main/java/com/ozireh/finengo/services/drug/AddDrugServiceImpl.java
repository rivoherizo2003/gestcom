package com.ozireh.finengo.services.drug;

import java.time.Instant;

import com.ozireh.finengo.model.Drug;
import org.springframework.stereotype.Service;

import com.ozireh.finengo.model.Category;
import com.ozireh.finengo.payloads.requests.DrugRequest;
import com.ozireh.finengo.repository.CategoryRepository;
import com.ozireh.finengo.repository.DrugRepository;

@Service
public class AddDrugServiceImpl implements AddDrugService {
    final
    DrugRepository drugRepository;

    final
    CategoryRepository categoryRepository;

    public AddDrugServiceImpl(DrugRepository drugRepository, CategoryRepository categoryRepository) {
        this.drugRepository = drugRepository;
        this.categoryRepository = categoryRepository;
    }

    public Drug execute(DrugRequest drugRequest) {
        Category category = categoryRepository.findById(drugRequest.getCategory()).orElse(null);
        Drug drug = new Drug();
        drug.setDrugCode(drugRequest.getCode());
        drug.setName(drugRequest.getName());
        drug.setPrice(drugRequest.getPrice());
        drug.setQuantity(drugRequest.getQuantity());
        drug.setCategory(category);
        drug.setDrugCode(drugRequest.getDrugCode());
        drug.setNotice(drugRequest.getNotice());
        drug.setExpirationDate(Instant.parse(drugRequest.getExpirationDate()));

        return drugRepository.save(drug);
    }

//    public Drug getById(Long id) {
//        return drugRepository.findById(id).orElse(null);
//    }
}
