package com.ozireh.finengo.services.projection;

import java.time.Instant;

public interface AccountInfo {

    Long getId();

    Instant getCreatedAt();

    Instant getUpdatedAt();

    String getUserCode();

    String getEmail();

    String getUsername();

    String getRealFileName();

    String getRoleName();
}
