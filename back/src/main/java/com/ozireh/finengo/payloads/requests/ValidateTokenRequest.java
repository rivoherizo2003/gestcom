package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidateTokenRequest {
    @NotBlank
    private String token;
}
