package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import lombok.Getter;

@Getter
public class AccountRequest {
    @NotBlank
    @Size(min = 4, max = 40)
    private String username;

    @NotBlank
    @Size(min = 6, max = 15)
    private String password;

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    private String[] roles;

    @Null
    private MediaRequest mediaRequest;
}
