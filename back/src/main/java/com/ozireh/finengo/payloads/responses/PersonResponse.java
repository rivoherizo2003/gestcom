package com.ozireh.finengo.payloads.responses;

public class PersonResponse {
    public Long id;
    public String firstname;
    public String lastname;
    public String gender;

    public String address;
    public String phoneNumber;

    public String fullName() {
        return String.format("%s %s", firstname, lastname).toString();
    }
}
