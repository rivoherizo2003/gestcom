package com.ozireh.finengo.payloads.responses;

import lombok.Getter;

@Getter
public class JwtAuthenticationResponse {
	public String accessToken;
	private final String usernameOrEmail;
	private final String fullName;
	private final Long media_id;
	private final Long id;
	private final String tokenType="Bearer";
	
	public JwtAuthenticationResponse(String accessToken, String usernameOrEmail, String fullName, Long mediaId, Long id) {
		this.accessToken = accessToken;
		this.usernameOrEmail = usernameOrEmail;
		this.fullName = fullName;
		this.media_id = mediaId;
		this.id = id;
	}
}
