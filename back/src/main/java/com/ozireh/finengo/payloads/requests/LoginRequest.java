package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.NotBlank;

import lombok.Getter;

@Getter
public class LoginRequest {
	
	@NotBlank
	private String usernameOrEmail;
	
	@NotBlank
	private String password;
	
	
}
