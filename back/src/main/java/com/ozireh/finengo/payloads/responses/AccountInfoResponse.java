package com.ozireh.finengo.payloads.responses;

import java.util.HashSet;

import com.ozireh.finengo.enumerator.RoleEnum;

public class AccountInfoResponse {
    public Long id;
    public String user_code;
    public String email;
    public String username;
    public HashSet<RoleEnum> roles = new HashSet<>();
    public HashSet<RoleEnum> systemRoles = new HashSet<>();
    public Long media_id;
    public String provider;
}
