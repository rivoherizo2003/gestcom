package com.ozireh.finengo.payloads.requests;

public class MediaRequest {
    public String fileBase64;

    public String fileName;

    public String filePath;
}
