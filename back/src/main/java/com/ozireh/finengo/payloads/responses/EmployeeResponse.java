package com.ozireh.finengo.payloads.responses;

public class EmployeeResponse implements DataResponse{
    public String position_title;
    public String education = "-";
    public String skills = "-";
    public String comment = "No comment";
    public PersonResponse person;
}
