package com.ozireh.finengo.payloads.responses;

import com.ozireh.finengo.enumerator.RoleEnum;

import java.util.HashSet;

public class PredefinedListResponse implements DataResponse{
    public HashSet<RoleEnum> roles;
}
