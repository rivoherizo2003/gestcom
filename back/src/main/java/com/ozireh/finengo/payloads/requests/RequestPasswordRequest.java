package com.ozireh.finengo.payloads.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class RequestPasswordRequest {
    @NotBlank
    private String email;
}
