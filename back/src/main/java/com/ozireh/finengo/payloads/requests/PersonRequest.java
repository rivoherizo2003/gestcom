package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.NotBlank;

import lombok.Getter;

@Getter
public class PersonRequest {
    @NotBlank
    private String gender;

    private String firstname;

    @NotBlank
    private String lastname;

    private String phone_number;

    private String address;

    private Long id;
}
