package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.NotNull;

import lombok.Getter;

@Getter
public class NewEmployeeRequest extends PersonRequest {

    private String position_title;

    private String comment;

    private String education;

    private String skills;

    @NotNull
    private AccountRequest account;
}
