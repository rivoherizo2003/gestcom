package com.ozireh.finengo.payloads.requests;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DrugRequest {
	@NotBlank
	@Size(min=3, max=100)
	private String name;

	@NotBlank
	@Size(min=3, max=10)
	private String code;

	@NotBlank
	@Size(min=3, max=50)
	private String drugCode;

	@NotBlank
	private String expirationDate;

	@NotBlank
	private String notice;
	
	private BigDecimal price;

	private float quantity;
	
	@NotNull
	private Long category;
}
