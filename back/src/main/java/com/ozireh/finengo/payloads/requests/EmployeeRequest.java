package com.ozireh.finengo.payloads.requests;

import lombok.Getter;

@Getter
public class EmployeeRequest extends PersonRequest{

    private String position_title;

    private String comment;

    private String education;

    private String skills;
}
