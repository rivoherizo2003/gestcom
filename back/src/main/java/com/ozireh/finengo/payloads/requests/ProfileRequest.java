package com.ozireh.finengo.payloads.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;

@Getter
public class ProfileRequest {

    // TODO: remove this payload or AccountRequest

    @NotBlank
    private Long id;

    @NotBlank
    @Email
    private String email;

    private String[] roles;

    @NotBlank
    private String username;
}
