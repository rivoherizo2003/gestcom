package com.ozireh.finengo.payloads.responses;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ApiResponse {
	private Boolean success;
	private String message;
	private String field;
	private HttpStatus http_status;
	private DataResponse data;

	public ApiResponse(boolean success, String message, String field, HttpStatus http_status) {
		this.success = success;
		this.message = message;
		this.field = field;
		this.http_status = http_status;
	}
	public ApiResponse(boolean success, String message, String field, HttpStatus http_status, DataResponse data) {
		this.success = success;
		this.message = message;
		this.field = field;
		this.http_status = http_status;
		this.data = data;
	}
}
