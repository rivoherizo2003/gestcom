package com.ozireh.finengo.payloads.responses;

public class UserProfileResponse implements DataResponse{
    public AccountInfoResponse account;
    public EmployeeResponse employee;
    public PersonResponse person;
}
