package com.ozireh.finengo.builder.account;

import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.payloads.responses.AccountInfoResponse;

import java.util.HashSet;

public interface AccountResponseBuilder {
    AccountResponseBuilder withIdentifier(Long id);

    AccountResponseBuilder withMail(String mail);

    AccountResponseBuilder withUsername(String username);

    AccountResponseBuilder withUserCode(String userCode);

    AccountResponseBuilder withUserRoles(HashSet<RoleEnum> roles);

    AccountResponseBuilder withMediaIdentifier(Long mediaIdentifier);

    AccountResponseBuilder withProvider(String provider);

    AccountInfoResponse build();
}
