package com.ozireh.finengo.builder.employee;

import com.ozireh.finengo.payloads.responses.EmployeeResponse;
import com.ozireh.finengo.payloads.responses.PersonResponse;
import org.springframework.stereotype.Service;

@Service
public class EmployeeResponseBuilderImpl implements EmployeeResponseBuilder {
    private String positionTitle;
    private String education = "-";
    private String skills = "-";
    private String comment = "No comment";
    private PersonResponse person;

    @Override
    public EmployeeResponseBuilder withPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
        return this;
    }

    @Override
    public EmployeeResponseBuilder withEducation(String education) {
        this.education = education;
        return this;
    }

    @Override
    public EmployeeResponseBuilder withSkills(String skills) {
        this.skills = skills;
        return this;
    }

    @Override
    public EmployeeResponseBuilder withComment(String comment) {
        this.comment = comment;
        return this;
    }

    @Override
    public EmployeeResponseBuilder withPerson(PersonResponse person) {
        this.person = person;
        return this;
    }

    @Override
    public EmployeeResponse build() {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.person = this.person;
        employeeResponse.comment = this.comment;
        employeeResponse.position_title = this.positionTitle;
        employeeResponse.education = this.education;
        employeeResponse.skills = this.skills;

        return employeeResponse;
    }


}
