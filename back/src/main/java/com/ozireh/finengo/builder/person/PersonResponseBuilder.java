package com.ozireh.finengo.builder.person;

import com.ozireh.finengo.payloads.responses.PersonResponse;

public interface PersonResponseBuilder {
    PersonResponseBuilder withId(Long id);

    PersonResponseBuilder withFirstname(String firstName);

    PersonResponseBuilder withLastname(String lastname);

    PersonResponseBuilder withGender(String gender);

    PersonResponseBuilder withAddress(String address);

    PersonResponseBuilder withPhoneNumber(String phoneNumber);

    PersonResponse build();
}
