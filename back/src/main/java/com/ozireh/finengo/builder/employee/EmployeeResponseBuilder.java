package com.ozireh.finengo.builder.employee;

import com.ozireh.finengo.payloads.responses.EmployeeResponse;
import com.ozireh.finengo.payloads.responses.PersonResponse;

public interface EmployeeResponseBuilder {
    EmployeeResponseBuilder withPositionTitle(String positionTitle);

    EmployeeResponseBuilder withEducation(String education);

    EmployeeResponseBuilder withSkills(String skills);

    EmployeeResponseBuilder withComment(String comment);

    EmployeeResponseBuilder withPerson(PersonResponse person);

    EmployeeResponse build();
}
