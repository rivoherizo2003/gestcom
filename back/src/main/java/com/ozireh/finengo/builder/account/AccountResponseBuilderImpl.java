package com.ozireh.finengo.builder.account;

import java.util.HashSet;

import org.springframework.stereotype.Service;

import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.payloads.responses.AccountInfoResponse;

@Service
public class AccountResponseBuilderImpl implements AccountResponseBuilder{
    private Long identifier;
    private String mail;
    private String username;
    private String userCode;
    private HashSet<RoleEnum> roles;
    private Long mediaIdentifier;
    private String provider;

    @Override
    public AccountResponseBuilder withIdentifier(Long id) {
        this.identifier = id;
        return this;
    }

    @Override
    public AccountResponseBuilder withMail(String mail) {
        this.mail = mail;
        return this;
    }

    @Override
    public AccountResponseBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    @Override
    public AccountResponseBuilder withUserCode(String userCode) {
       this.userCode = userCode;
        return this;
    }

    @Override
    public AccountResponseBuilder withUserRoles(HashSet<RoleEnum> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public AccountResponseBuilder withMediaIdentifier(Long mediaIdentifier) {
        this.mediaIdentifier = mediaIdentifier;
        return this;
    }

    @Override
    public AccountResponseBuilder withProvider(String provider) {
        this.provider = provider;
        return this;
    }

    @Override
    public AccountInfoResponse build() {
        AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.id = this.identifier;
        accountInfoResponse.username = this.username;
        accountInfoResponse.user_code = this.userCode;
        accountInfoResponse.email = this.mail;
        accountInfoResponse.roles = this.roles;
        accountInfoResponse.media_id = this.mediaIdentifier;
        accountInfoResponse.provider = this.provider;

        return accountInfoResponse;
    }
}
