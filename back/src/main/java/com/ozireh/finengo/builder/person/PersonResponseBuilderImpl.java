package com.ozireh.finengo.builder.person;

import com.ozireh.finengo.payloads.responses.PersonResponse;
import org.springframework.stereotype.Service;

@Service
public class PersonResponseBuilderImpl implements PersonResponseBuilder {
    private Long id;
    private String firstname;
    private String lastname;
    private String gender;
    private String address;
    private String phoneNumber;

    @Override
    public PersonResponseBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public PersonResponseBuilder withFirstname(String firstName) {
        this.firstname = firstName;
        return this;
    }

    @Override
    public PersonResponseBuilder withLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    @Override
    public PersonResponseBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public PersonResponseBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public PersonResponseBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    @Override
    public PersonResponse build() {
        PersonResponse personResponse = new PersonResponse();
        personResponse.id =this.id;
        personResponse.firstname = this.firstname;
        personResponse.lastname = this.lastname;
        personResponse.gender = this.gender;
        personResponse.address = this.address;
        personResponse.phoneNumber = this.phoneNumber;

        return personResponse;
    }
}
