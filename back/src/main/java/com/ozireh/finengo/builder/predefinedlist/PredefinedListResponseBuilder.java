package com.ozireh.finengo.builder.predefinedlist;

import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.payloads.responses.PredefinedListResponse;

import java.util.HashSet;

public interface PredefinedListResponseBuilder {
    PredefinedListResponseBuilder withRoles(HashSet<RoleEnum> roles);

    PredefinedListResponse build();
}
