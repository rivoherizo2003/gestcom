package com.ozireh.finengo.builder.predefinedlist;

import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.payloads.responses.PredefinedListResponse;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class PredefinedListResponseBuilderImpl implements PredefinedListResponseBuilder{
    private HashSet<RoleEnum> roles;

    @Override
    public PredefinedListResponseBuilder withRoles(HashSet<RoleEnum> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public PredefinedListResponse build() {
        PredefinedListResponse predefinedListResponse = new PredefinedListResponse();
        predefinedListResponse.roles = this.roles;

        return predefinedListResponse;
    }


}
