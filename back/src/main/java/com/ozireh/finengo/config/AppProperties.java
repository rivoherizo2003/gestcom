package com.ozireh.finengo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    public String urlFrontResetPwd;

    public String getUrlFrontResetPwd() {
        return urlFrontResetPwd;
    }

    public void setUrlFrontResetPwd(String urlFrontResetPwd) {
        this.urlFrontResetPwd = urlFrontResetPwd;
    }
}
