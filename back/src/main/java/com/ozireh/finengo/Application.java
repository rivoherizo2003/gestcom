package com.ozireh.finengo;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.core.SpringVersion;
import org.springframework.data.convert.Jsr310Converters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
// @EntityScan("com.ozireh.finengo.model")
@EntityScan(basePackageClasses = {
		Application.class,
		Jsr310Converters.class
})
@EnableJpaRepositories("com.ozireh.finengo.repository")
public class Application {
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		System.out.println("Spring version =>"+SpringVersion.getVersion());
	}
}
