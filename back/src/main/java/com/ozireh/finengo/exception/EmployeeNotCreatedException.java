package com.ozireh.finengo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class EmployeeNotCreatedException extends RuntimeException {
    public EmployeeNotCreatedException(String message){
        super(message);
    }

    public EmployeeNotCreatedException(String message, Throwable cause){
        super(message, cause);
    }
}
