package com.ozireh.finengo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ozireh.finengo.model.audit.DateAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "gc_media")
public class Media extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    private String realFileName;

    @Column(nullable = false)
    private String extension;

    @Column(nullable = false)
    private String filePath;

    private Media(MediaBuilder mediaBuilder){
        this.extension = mediaBuilder.extension;
        this.fileName = mediaBuilder.fileName;
        this.realFileName = mediaBuilder.realFileName;
        this.filePath = mediaBuilder.filePath;
    }

    public static MediaBuilder getBuilder() {
        return new MediaBuilder();
    }

    public static class MediaBuilder{
        private String fileName;

        private String realFileName;

        private String extension;

        private String filePath;

        public MediaBuilder withFileName(String fileName){
            this.fileName = fileName;

            return this;
        }

        public MediaBuilder withRealFileName(String realFileName){
            this.realFileName = realFileName;

            return this;
        }

        public MediaBuilder withExtension(String extension){
            this.extension = extension;

            return this;
        }

        public MediaBuilder withFilePath(String filePath){
            this.filePath = filePath;

            return this;
        }

        public Media build(){
            return new Media(this);
        }

        public Media build(Media media) {
            if(null != this.fileName) media.fileName = this.fileName;
            if(null != this.realFileName) media.realFileName = this.realFileName;
            if(null != this.extension) media.extension = this.extension;
            if(null != this.filePath) media.filePath = this.filePath;

            return media;
        }
    }
}
