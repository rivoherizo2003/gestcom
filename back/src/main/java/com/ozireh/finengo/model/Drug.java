package com.ozireh.finengo.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("drug")
public class Drug extends Product{
    @Column(length = 20)
    private String drugCode;

    @Column
    private String notice;

    @Column
    private Instant expirationDate;
}
