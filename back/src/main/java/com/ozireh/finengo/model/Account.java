package com.ozireh.finengo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.ozireh.finengo.enumerator.ProviderEnum;
import com.ozireh.finengo.model.audit.DateAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username"}),
        @UniqueConstraint(columnNames = {"email"})
}, name = "gc_account")
public class Account extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String userCode;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false, length = 100)
    private String password;

    @ManyToMany
    @JoinTable(name = "gc_account_roles", joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "media_id", referencedColumnName = "id")
    private Media media;

    @Enumerated(EnumType.STRING)
    private ProviderEnum provider;

    public Account(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    private Account(AccountBuilder accountBuilder) {
        this.username = accountBuilder.username;
        this.email = accountBuilder.email;
        this.password = accountBuilder.password;
        this.roles = accountBuilder.roles;
        this.media = accountBuilder.media;
        this.userCode = accountBuilder.userCode;
    }

    public static AccountBuilder getBuilder() {
        return new AccountBuilder();
    }

    public static class AccountBuilder {
        private String userCode;
        private String username;
        private String email;
        private String password;
        private ProviderEnum provider;
        private Set<Role> roles;
        private Media media;

        public AccountBuilder withUsername(String username) {
        	this.username = username;

        	return this;
		}

		public AccountBuilder withEmail(String email) {
			this.email = email;

			return this;
		}

		public AccountBuilder withPassword(String password) {
			this.password = password;

			return this;
		}

		public AccountBuilder withRoles(Set<Role> roles) {
			this.roles = roles;

			return this;
		}

		public AccountBuilder withProvider(ProviderEnum provider) {
			this.provider = provider;

			return this;
		}

		public AccountBuilder withMedia(Media media) {
			this.media = media;

			return this;
		}

        public AccountBuilder withUserCode(String userCode) {
            this.userCode = userCode;

            return this;
        }

        public Account build() {
            return new Account(this);
        }

        public Account build(Account account) {
			if(null != this.username) account.username = this.username;
			if(null != this.email) account.email = this.email;
			if(null != this.password) account.password = this.password;
			if(null != this.provider) account.provider = this.provider;
			if(null != this.roles) account.roles = this.roles;
			if(null != this.media) account.media = this.media;
			if(null != this.userCode) account.userCode = this.userCode;

			return account;
        }
    }
}
