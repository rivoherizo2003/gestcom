package com.ozireh.finengo.model;

import com.ozireh.finengo.enumerator.GenderEnum;
import com.ozireh.finengo.model.audit.UserDateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "disc")
@Table(name = "gc_person")
public abstract class Person extends UserDateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    protected Long id;

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "last_name")
    protected String lastName;

    @Column(name = "address")
    protected String address;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    protected GenderEnum gender;

    @Column(name = "phone_number")
    protected String phoneNumber;
}
