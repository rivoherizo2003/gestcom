package com.ozireh.finengo.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class Mail {
    private String from;
    private String mailTo;
    private String subject;
    private Map<String, Object> props;
    private String templateName;
}
