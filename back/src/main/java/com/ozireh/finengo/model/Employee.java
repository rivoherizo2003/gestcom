package com.ozireh.finengo.model;

import com.ozireh.finengo.enumerator.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("employee")
public class Employee extends Person{
    @Column(length = 100)
    private String comment;

    @Column(columnDefinition = "TEXT")
    private String skills;

    @Column(columnDefinition = "TEXT")
    private String education;

    @Column(length = 50, name = "position_title")
    private String positionTitle;

    @OneToOne
    private Account account;

    private Employee(EmployeeBuilder employeeBuilder){
        this.comment = employeeBuilder.comment;
        this.skills = employeeBuilder.skills;
        this.education = employeeBuilder.education;
        this.positionTitle = employeeBuilder.positionTitle;

        this.firstName = employeeBuilder.firstName;
        this.lastName = employeeBuilder.lastName;
        this.address = employeeBuilder.address;
        this.gender = employeeBuilder.gender;
        this.phoneNumber = employeeBuilder.phoneNumber;
    }

    public static EmployeeBuilder getBuilder(){
        return new EmployeeBuilder();
    }

    public static class EmployeeBuilder{
        private String firstName;
        private String lastName;
        private String address;
        private GenderEnum gender;
        private String phoneNumber;
        private String comment;
        private String skills;
        private String education;
        private String positionTitle;

        public EmployeeBuilder withComment(String comment){
            this.comment = comment;
            return this;
        }

        public EmployeeBuilder withSkills(String skills){
            this.skills = skills;
            return this;
        }

        public EmployeeBuilder withEducation(String education){
            this.education = education;
            return this;
        }

        public EmployeeBuilder withPositionTitle(String positionTitle){
            this.positionTitle = positionTitle;
            return this;
        }

        public EmployeeBuilder withFirstname(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder withLastname(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeBuilder withAddress(String address) {
            this.address = address;
            return this;
        }

        public EmployeeBuilder withGender(String gender) {
            this.gender = GenderEnum.getByValue(gender);
            return this;
        }

        public EmployeeBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Employee build(){
            return new Employee(this);
        }

        public Employee build(Employee employee){
            if (null != this.comment) employee.comment = this.comment;
            if (null != this.skills) employee.skills = this.skills;
            if (null != this.education) employee.education = this.education;
            if (null != this.positionTitle) employee.positionTitle = this.positionTitle;

            if (null != this.firstName) employee.firstName = this.firstName;
            if (null != this.firstName) employee.lastName = this.lastName;
            if (null != this.address) employee.address = this.address;
            if (null != this.gender) employee.gender = this.gender;
            if (null != this.phoneNumber) employee.phoneNumber = this.phoneNumber;

            return employee;
        }
    }
}
