package com.ozireh.finengo.dao;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ozireh.finengo.model.Media;
import com.ozireh.finengo.repository.MediaRepository;


@Service
public class MediaDaoImpl implements MediaDao {

	private final MediaRepository mediaRepository;

	public MediaDaoImpl(MediaRepository mediaRepository) {
		this.mediaRepository = mediaRepository;
	}

	@Override
	public Optional<Media> getById(Long id) {
		return mediaRepository.findById(id);
	}

	@Override
	public Media saveMedia(Media media) {
		return mediaRepository.save(media);
	}

	@Override
	public void deleteMedia(Media media) {
		mediaRepository.delete(media);
	}
}
