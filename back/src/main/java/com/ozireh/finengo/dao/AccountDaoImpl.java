package com.ozireh.finengo.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.services.projection.AccountInfo;


@Service
public class AccountDaoImpl implements AccountDao {
	private final AccountRepository accountRepository;

	public AccountDaoImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	@Override
	public Optional<Account> getById(Long id) {
		return accountRepository.findById(id);
	}

	@Override
	public Optional<Account> findByEmail(String email) {
		return accountRepository.findByEmail(email);
	}

	@Override
	public Optional<Account> findByUsernameOrEmail(String usernameOrEmail) {
		return accountRepository.findByUsernameOrEmail(usernameOrEmail);
	}

	@Override
	public Boolean existByUsername(String username) {
		return accountRepository.existsByUsername(username);
	}
	@Override
	public Boolean existByEmail(String email) {
		return accountRepository.existsByEmail(email);
	}

	public Optional<Account> loadByUsernameOrEmail(String email, String username){
		return accountRepository.findByUsernameOrEmail(email);
	}

	/**
	 * paginate over accounts
	 */
	public Page<AccountInfo> paginateUsers(int page, int size){
		Pageable paging = PageRequest.of(page, size);
		Page<AccountInfo> pageAccountInfos;
		pageAccountInfos = accountRepository.findAllUsersAndPaginate(paging);

		return pageAccountInfos;
	}


	@Override
	public boolean isAccountExistsWithEmailNotWithAccountId(String email, Long accountId) {
		return accountRepository.existsAccountByEmailAndIdNot(email, accountId);
	}

	@Override
	public boolean isAccountExistsWithUsernameNotWithAccountId(String username, Long accountId) {
		return accountRepository.existsAccountByUsernameAndIdNot(username, accountId);
	}
	
	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void delete(Account account) {
		accountRepository.delete(account);
	}
}
