package com.ozireh.finengo.dao;


import java.util.Optional;

import com.ozireh.finengo.model.Media;

public interface MediaDao {

	/**
	 * @param id media id
	 */
	Optional<Media> getById(Long id);

	public Media saveMedia(Media media);

	public void deleteMedia(Media media);
}
