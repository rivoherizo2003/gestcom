package com.ozireh.finengo.dao;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeDaoImpl implements EmployeeDao {
    private final EmployeeRepository employeeRepository;

    public EmployeeDaoImpl(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Optional<Employee> findEmployeeByAccount(Account account) {
        return employeeRepository.findByAccount(account);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void delete(Employee employee) {
        employeeRepository.delete(employee);
    }
}
