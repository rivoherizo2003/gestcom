package com.ozireh.finengo.dao;

import java.util.HashSet;
import java.util.List;

import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.enumerator.RoleEnum;

public interface RoleDao {
	Role findByName(RoleEnum roleName);

	HashSet<Role> findNames(List<String> roles);

	List<Role> getAll();

	HashSet<RoleEnum> findAllRoleNames();
}
