package com.ozireh.finengo.dao;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;

import java.util.Optional;

public interface EmployeeDao {
    /**
     * @param account account to retrieve
     * @return Optional<Employee>>
     */
    Optional<Employee> findEmployeeByAccount(Account account);

    /**
     * @param employee the employee
     * @return Employee
     */
    Employee save(Employee employee);

    /**
     * @param employee an employee
     */
    void delete(Employee employee);
}
