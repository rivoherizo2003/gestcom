package com.ozireh.finengo.dao;

import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.enumerator.RoleEnum;
import com.ozireh.finengo.repository.RoleRepository;

@Service
public class RoleDaoImpl implements RoleDao {
	private final RoleRepository roleRepository;

	public RoleDaoImpl(RoleRepository roleRepository){
		this.roleRepository = roleRepository;
	}
	
	public Role findByName(RoleEnum roleName) {
		return this.roleRepository.findByName(roleName).get();
	}
	
	public HashSet<Role> findNames(List<String> roles) {
		return this.roleRepository.findNames(roles);
	}
	
	@Override
	public List<Role> getAll() {
		return this.roleRepository.findAll();
	}

	public HashSet<RoleEnum> findAllRoleNames(){ return  this.roleRepository.findRoleNames();}
}
