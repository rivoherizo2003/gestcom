package com.ozireh.finengo.dao;


import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.services.projection.AccountInfo;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface AccountDao {

	/**
	 * @param id account id
	 */
	Optional<Account> getById(Long id);

	/**
	 * @param email account email
	 * @return Optional<Account>
	 */
	Optional<Account> findByEmail(String email);

	/**
	 *
	 * @param usernameOrEmail username or email
	 * @return Optional<Account>
	 */
	Optional<Account> findByUsernameOrEmail(String usernameOrEmail);

	/**
	 *
	 * @param username username
	 * @return Boolean
	 */
	Boolean existByUsername(String username);

	/**
	 * @param email email account
	 * @return Boolean
	 */
	Boolean existByEmail(String email);

	/**
	 *
	 * @param page page
	 * @param size size
	 * @return Page<AccountInfo>
	 */
    Page<AccountInfo> paginateUsers(int page, int size);

	/**
	 * Check if the email is already associated with an account, not including the current
	 * account connected
	 * @param email email
	 * @param accountId logged account
	 * @return Boolean
	 */
    boolean isAccountExistsWithEmailNotWithAccountId(String email,  Long accountId);

	/**
	 * check if username is associated with another account and not considering the logged account
	 * @param username username
	 * @param accountId logged account
	 * @return Boolean
	 */
    boolean isAccountExistsWithUsernameNotWithAccountId(String username,  Long accountId);


	/**
	 * save account
	 * @param account account
	 * @return Account
	 */
	public Account save(Account account);

	/**
	 * delete an account
	 * @param account account to delete
	 */
	public void delete(Account account);
}
