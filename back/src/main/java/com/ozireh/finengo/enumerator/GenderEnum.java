package com.ozireh.finengo.enumerator;

public enum GenderEnum {
    M("Man", "M"),
    W("Woman", "W");

    private final String title;
    private final String value;

    GenderEnum(String title, String value) {
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public static GenderEnum getByValue(String value) {
        for (GenderEnum genderEnum : GenderEnum.values()) {
            if(genderEnum.getValue().equals(value)){
                return genderEnum;
            }
        }

        return null;
    }
}
