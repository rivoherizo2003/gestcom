package com.ozireh.finengo.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ozireh.finengo.exception.ResourceNotFoundException;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.repository.AccountRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private final AccountRepository accountRepository;

	public CustomUserDetailsService(AccountRepository accountRepository){
		this.accountRepository = accountRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		Account account = this.accountRepository.findByUsernameOrEmail(usernameOrEmail)
				.orElseThrow(() ->
		new UsernameNotFoundException("Account not found with username or email : " + usernameOrEmail));
		
		return UserPrincipal.create(account);
	}
	
	@Transactional
	public UserDetails loadUserById(Long id) {
		Account account = this.accountRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("User", "id", id));

		return UserPrincipal.create(account);
	}
}
