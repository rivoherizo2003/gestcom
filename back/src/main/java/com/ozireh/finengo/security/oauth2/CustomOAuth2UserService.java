package com.ozireh.finengo.security.oauth2;

import com.ozireh.finengo.enumerator.ProviderEnum;
import com.ozireh.finengo.exception.OAuth2AuthenticationProcessingException;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.security.UserPrincipal;
import com.ozireh.finengo.security.oauth2.user.OAuth2UserFactory;
import com.ozireh.finengo.security.oauth2.user.OAuth2UserInfo;
import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.services.account.ProcessOauth2Service;
import com.ozireh.finengo.util.ProviderUtils;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    private final ProcessOauth2Service processOauth2Service;
    private final AccountDao getAccountService;

    public CustomOAuth2UserService(ProcessOauth2Service processOauth2Service, AccountDao getAccountService) {

        this.processOauth2Service = processOauth2Service;
        this.getAccountService = getAccountService;
    }

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User auth2User = super.loadUser(userRequest);

        try {
            return processOAuthUser(userRequest, auth2User);
        } catch (Exception e){
            throw new InternalAuthenticationServiceException(e.getMessage(), e.getCause());
        }
    }

    private OAuth2User processOAuthUser(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        Account ac;
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());

        ProviderEnum providerEnum = ProviderUtils.valueOfLabel(oAuth2UserRequest.getClientRegistration().getRegistrationId());
        if (null == oAuth2UserInfo) {
            throw new OAuth2AuthenticationProcessingException("Email not found from auth2 provider");
        }
        Optional<Account> accountOptional = getAccountService.findByEmail(oAuth2UserInfo.getEmail());

        if (accountOptional.isPresent()) {
            ac = accountOptional.get();
            return UserPrincipal.create(ac, oAuth2UserInfo.getAttributes());
        }

        return UserPrincipal.create(processOauth2Service.execute(oAuth2UserInfo.getEmail(), providerEnum));
    }
}
