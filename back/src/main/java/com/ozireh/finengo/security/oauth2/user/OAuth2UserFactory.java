package com.ozireh.finengo.security.oauth2.user;

import com.ozireh.finengo.enumerator.ProviderEnum;
import com.ozireh.finengo.exception.OAuth2AuthenticationProcessingException;

import java.util.Map;

public class OAuth2UserFactory {
    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(ProviderEnum.google.toString())) {
            return new GoogleOAuth2UserInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(ProviderEnum.facebook.toString())) {
            return new FacebookOAuth2UserInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(ProviderEnum.github.toString())) {
            return null;
        } else {
            throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
