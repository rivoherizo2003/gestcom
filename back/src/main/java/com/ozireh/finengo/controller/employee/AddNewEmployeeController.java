package com.ozireh.finengo.controller.employee;

import javax.annotation.Nullable;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ozireh.finengo.controller.BaseController;
import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.model.Media;
import com.ozireh.finengo.payloads.requests.NewEmployeeRequest;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.responses.EmployeeResponse;
import com.ozireh.finengo.services.account.CreateAccountService;
import com.ozireh.finengo.services.employee.CreateEmployeeResponseService;
import com.ozireh.finengo.services.employee.CreateEmployeeService;
import com.ozireh.finengo.services.media.SaveMediaProfileAccountService;

@RestController
@RequestMapping("/api")
public class AddNewEmployeeController extends BaseController {
    private final AccountDao accountDao;
    private final CreateEmployeeService createEmployeeService;
    private final CreateAccountService createAccountService;
    private final SaveMediaProfileAccountService saveMediaProfileAccountService;
    private final CreateEmployeeResponseService createEmployeeResponseService;

    public AddNewEmployeeController(AccountDao accountDao, CreateEmployeeService createEmployeeService, CreateAccountService createAccountService, SaveMediaProfileAccountService saveMediaProfileAccountService, CreateEmployeeResponseService createEmployeeResponseService) {
        this.accountDao = accountDao;
        this.createEmployeeService = createEmployeeService;
        this.createAccountService = createAccountService;
        this.saveMediaProfileAccountService = saveMediaProfileAccountService;
        this.createEmployeeResponseService = createEmployeeResponseService;
    }

    @PostMapping(value = "/add/employee")
    public ResponseEntity<?> addNewEmployee(@Valid @RequestParam("new_employee_request") String employeeData, @Nullable @RequestParam("profile_img") MultipartFile profileImg) throws Exception {
        NewEmployeeRequest newEmployeeRequest = requestMapper(employeeData, NewEmployeeRequest.class);
        Boolean isAccountExists = accountDao.existByEmail(newEmployeeRequest.getAccount().getEmail());
        if (!isAccountExists) {
            Employee employee = createEmployeeService.create(newEmployeeRequest);
            Account account = createAccountService.create(newEmployeeRequest.getAccount());
            if (null != profileImg){
                Media media = saveMediaProfileAccountService.save(profileImg, account);
            }
            EmployeeResponse employeeResponse = createEmployeeResponseService.createFromEmployee(employee);
            return new ResponseEntity<>(new ApiResponse(true, "Account info saved!", "", HttpStatus.ACCEPTED, employeeResponse), HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(new ApiResponse(false, "An account already exists with this email " + newEmployeeRequest.getAccount().getEmail(), "", HttpStatus.UNPROCESSABLE_ENTITY), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
