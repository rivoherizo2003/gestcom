package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.requests.RequestPasswordRequest;
import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.services.account.SendMailResetPasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/security")
public class RequestPasswordController {
    private final AccountDao accountDao;
    private final SendMailResetPasswordService sendMailResetPassword;

    public RequestPasswordController(AccountDao accountDao, SendMailResetPasswordService sendMailResetPassword){

        this.accountDao = accountDao;
        this.sendMailResetPassword = sendMailResetPassword;
    }

    @PostMapping("/request-new-password")
    public ResponseEntity<?> requestPassword(@Valid @RequestBody RequestPasswordRequest requestPasswordRequest) throws MessagingException {
        if(accountDao.existByEmail(requestPasswordRequest.getEmail())){
            Optional<Account> account = accountDao.findByEmail(requestPasswordRequest.getEmail());
            if (account.isPresent()) sendMailResetPassword.sendMailResponse(account.get());

            return new ResponseEntity<>(new ApiResponse(true, "Check your mail " + requestPasswordRequest.getEmail() + " and clic on the link to change your password", "email", HttpStatus.ACCEPTED),  HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(new ApiResponse(false, "Unable to find account linked to this mail "+ requestPasswordRequest.getEmail(), "email", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
}
