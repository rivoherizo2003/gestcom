package com.ozireh.finengo.controller.account;

import java.util.Optional;

import javax.annotation.Nullable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.repository.AccountRepository;

@RestController
public class IsUsernameAvailableController {
    private final AccountDao accountService;

    public IsUsernameAvailableController(AccountDao accountService, AccountRepository accountRepository){
        this.accountService = accountService;
    }

    @GetMapping("/api/is-username-available")
    public ResponseEntity<?> checkUsernameAvailabilityForAccountConnected(@RequestParam String username, @RequestParam @Nullable Long accountId){
        Optional<Account> optionalAccount = accountService.findByUsernameOrEmail(username);
        boolean bIsUsernameAvailable = true;

        if (optionalAccount.isPresent()){
            bIsUsernameAvailable = null != accountId && !accountService.isAccountExistsWithUsernameNotWithAccountId(username, accountId);
        }
        if(!bIsUsernameAvailable)
            return new ResponseEntity<>(new ApiResponse(false, "Username is already in use", "Username", HttpStatus.ACCEPTED),
                    HttpStatus.ACCEPTED);

        return new ResponseEntity<>(new ApiResponse(true, "Username is availbable","Username", HttpStatus.ACCEPTED),
                HttpStatus.ACCEPTED);
    }
}
