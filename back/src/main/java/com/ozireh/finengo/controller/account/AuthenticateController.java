package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.AccountInfoResponse;
import com.ozireh.finengo.payloads.responses.JwtAuthenticationResponse;
import com.ozireh.finengo.payloads.requests.LoginRequest;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.security.JwtTokenProvider;
import com.ozireh.finengo.services.account.CreateAccountResponseService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/security")
public class AuthenticateController {
	private final AuthenticationManager authenticationManager;
	private final JwtTokenProvider jwtTokenProvider;
	private final CreateAccountResponseService createAccountResponseByAccount;
	private final AccountRepository accountRepository;

	public AuthenticateController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, CreateAccountResponseService buildAccountResponseByAccount, AccountRepository accountRepository){
		this.authenticationManager = authenticationManager;
		this.jwtTokenProvider = jwtTokenProvider;
		this.createAccountResponseByAccount = buildAccountResponseByAccount;
		this.accountRepository = accountRepository;
	}

	@PostMapping("/authenticate")
	public ResponseEntity<?> authenticateAccount(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtTokenProvider.generateToken(authentication, 0);
		Optional<Account> accountOptional = accountRepository.findByUsernameOrEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		if (accountOptional.isPresent()){
			AccountInfoResponse accountInfoResponse = createAccountResponseByAccount.create(accountOptional.get());
			return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, loginRequest.getUsernameOrEmail(), accountInfoResponse.username, accountInfoResponse.media_id, accountInfoResponse.id));
		}

		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, loginRequest.getUsernameOrEmail(), "", 0L, 0L));
	}
}
