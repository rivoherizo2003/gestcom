package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.requests.ValidateTokenRequest;
import com.ozireh.finengo.security.JwtTokenProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ValidateTokenController {
    private final JwtTokenProvider jwtTokenProvider;

    public ValidateTokenController(JwtTokenProvider jwtTokenProvider){

        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/auth/validate/token")
    public ResponseEntity<?> validateToken(@Valid @RequestBody ValidateTokenRequest validateTokenRequest){
        if (jwtTokenProvider.validateToken(validateTokenRequest.getToken())){
            return new ResponseEntity<>(new ApiResponse(true, "Token is valid", "token", HttpStatus.ACCEPTED),
                    HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(new ApiResponse(false, "Invalid token", "token", HttpStatus.UNAUTHORIZED),
                HttpStatus.UNAUTHORIZED);
    }
}
