package com.ozireh.finengo.controller.drug;

import java.net.URI;

import javax.validation.Valid;

import com.ozireh.finengo.services.drug.AddDrugService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ozireh.finengo.model.Product;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.requests.DrugRequest;


@RestController
public class DrugAddController {
	final
	AddDrugService addDrugServiceImpl;

	public DrugAddController(AddDrugService addDrugServiceImpl) {
		this.addDrugServiceImpl = addDrugServiceImpl;
	}

	@PostMapping("/api/drug/add")
	public ResponseEntity<?> execute(@Valid @RequestBody DrugRequest drugRequest) {
		Product product = this.addDrugServiceImpl.execute(drugRequest);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/auth/add")
				.buildAndExpand().toUri();
		
		return ResponseEntity.created(location).body(new ApiResponse(true, "Product "+product.getName()+" created successfully", "", HttpStatus.CREATED));
	}
}
