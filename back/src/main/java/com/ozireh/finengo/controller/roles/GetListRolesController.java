package com.ozireh.finengo.controller.roles;

import java.util.HashSet;

import com.ozireh.finengo.enumerator.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ozireh.finengo.dao.RoleDao;

@RestController
@RequestMapping("/api/auth")
public class GetListRolesController {
	
	@Autowired
    RoleDao roleDao;
	
	@GetMapping("/role/list")
	public HashSet<RoleEnum> list(){
		return roleDao.findAllRoleNames();
	}
}
