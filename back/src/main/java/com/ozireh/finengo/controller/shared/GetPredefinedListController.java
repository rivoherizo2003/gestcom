package com.ozireh.finengo.controller.shared;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ozireh.finengo.services.CreatePredefinedListResponseService;

@RestController
@RequestMapping("/api")
public class GetPredefinedListController {

    private final CreatePredefinedListResponseService createPredefinedListResponseService;

    GetPredefinedListController(CreatePredefinedListResponseService createPredefinedListResponseService){
        this.createPredefinedListResponseService = createPredefinedListResponseService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> getPredefinedList(){
        return ResponseEntity.ok(createPredefinedListResponseService.create());
    }
}
