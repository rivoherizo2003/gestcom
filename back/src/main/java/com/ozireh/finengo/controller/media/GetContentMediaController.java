package com.ozireh.finengo.controller.media;

import com.ozireh.finengo.model.Media;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.repository.MediaRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class GetContentMediaController {
    private final MediaRepository mediaRepository;

    public GetContentMediaController(MediaRepository mediaRepository){

        this.mediaRepository = mediaRepository;
    }

    @GetMapping("/content/media")
    public ResponseEntity<?> getContent(@RequestParam("id") Long id){
        Optional<Media> mediaOptional = mediaRepository.findById(id);
        if(mediaOptional.isPresent()) {
            Media media = mediaOptional.get();
            byte[] content = new byte[0];

            try{
                content = FileUtils.readFileToByteArray(new File(media.getFilePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            // TODO register mimetype in table media in order to make this more dynamic
            return ResponseEntity.ok().contentType(MediaType.valueOf("image/"+media.getExtension())).body(content);
        }

        return new ResponseEntity<>(new ApiResponse(false, "Media not found", "", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
}
