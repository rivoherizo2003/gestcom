package com.ozireh.finengo.controller.account;

import java.util.Optional;

import javax.annotation.Nullable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.repository.AccountRepository;

@RestController
public class IsEmailAccountAvailableController {
    private final AccountDao accountDao;

    public IsEmailAccountAvailableController(AccountDao accountService, AccountRepository accountRepository) {
        this.accountDao = accountService;
    }

    @GetMapping("/api/is-mail-available")
    public ResponseEntity<?> checkMailAvailability(@RequestParam String email, @RequestParam @Nullable Long accountId) {
        Optional<Account> optionalAccount = accountDao.findByUsernameOrEmail(email);
        boolean bIsEmailAvailable = true;
        if (optionalAccount.isPresent()) {
             bIsEmailAvailable = null != accountId && !accountDao.isAccountExistsWithEmailNotWithAccountId(email, accountId);
        }

        if (!bIsEmailAvailable){
            return new ResponseEntity<>(new ApiResponse(false, "Mail is already in use", "mail", HttpStatus.NOT_ACCEPTABLE),
                    HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(new ApiResponse(true, "Mail is availbable", "mail", HttpStatus.ACCEPTED),
                HttpStatus.ACCEPTED);
    }
}
