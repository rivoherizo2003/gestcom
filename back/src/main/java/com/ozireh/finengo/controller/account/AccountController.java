package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.services.projection.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/security")
public class AccountController {

	@Autowired
	AccountDao accountService;

	@GetMapping("/list/accounts")
	public ResponseEntity<Map<String, Object>> getAccounts(@RequestParam(defaultValue = "0") int page,
														   @RequestParam(defaultValue = "3") int size){
		try{
			Page<AccountInfo> pageAccountInfos = accountService.paginateUsers(page, size);
			Map<String, Object> response = new HashMap<>();
			response.put("accountInfos", pageAccountInfos.getContent());
			response.put("currentPage", pageAccountInfos.getNumber());
			response.put("totalItems", pageAccountInfos.getTotalElements());
			response.put("totalPages", pageAccountInfos.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
