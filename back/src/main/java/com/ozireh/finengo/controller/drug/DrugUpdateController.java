package com.ozireh.finengo.controller.drug;

import com.ozireh.finengo.payloads.requests.DrugRequest;
import com.ozireh.finengo.services.drug.AddDrugService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class DrugUpdateController {
	final
	AddDrugService addDrugService;

	public DrugUpdateController(AddDrugService addDrugService) {
		this.addDrugService = addDrugService;
	}

	@GetMapping("/api/drug/update/{id}")
	public ResponseEntity<?> execute(@RequestBody DrugRequest drugRequest, @PathVariable("id") Integer id) {
		this.addDrugService.execute(drugRequest);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
