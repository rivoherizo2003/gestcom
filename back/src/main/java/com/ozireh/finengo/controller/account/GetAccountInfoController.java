package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.responses.UserProfileResponse;
import com.ozireh.finengo.services.account.CreateUserProfileResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GetAccountInfoController {
    private final CreateUserProfileResponseService createUserProfileResponseService;

    public GetAccountInfoController(CreateUserProfileResponseService buildUserProfileResponse){
        this.createUserProfileResponseService = buildUserProfileResponse;
    }

    @GetMapping("/account-info")
    public ResponseEntity<?> getAccountInfo(@RequestParam("id") Long id){
        UserProfileResponse userProfileResponse = createUserProfileResponseService.createFromAccountId(id);
        if (null != userProfileResponse) {
            return new ResponseEntity<>(new ApiResponse(false, "Account info", "", HttpStatus.NOT_FOUND, userProfileResponse), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new ApiResponse(false, "Account info", "", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
}
