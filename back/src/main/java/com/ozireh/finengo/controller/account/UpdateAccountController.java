package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.controller.BaseController;
import com.ozireh.finengo.dao.AccountDao;
import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Media;
import com.ozireh.finengo.payloads.requests.ProfileRequest;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.services.account.UpdateAccountService;
import com.ozireh.finengo.services.media.SaveMediaProfileAccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nullable;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UpdateAccountController extends BaseController {
    private final AccountDao accountDao;
    private final SaveMediaProfileAccountService saveMediaProfileAccountService;
    private final UpdateAccountService updateAccountProfileService;

    public UpdateAccountController(AccountDao accountDao, SaveMediaProfileAccountService saveMediaProfileAccountService, UpdateAccountService updateProfileService){
        this.accountDao = accountDao;
        this.saveMediaProfileAccountService = saveMediaProfileAccountService;
        this.updateAccountProfileService = updateProfileService;
    }

    @PutMapping(value = "/update/account")
    public ResponseEntity<?> updateAccount(@Valid @RequestParam("profile_request") String profileData, @Nullable @RequestParam("profile_img") MultipartFile profileImg) throws Exception {
        ProfileRequest profileRequest = requestMapper(profileData, ProfileRequest.class);
        Optional<Account> optionalAccount = accountDao.getById(profileRequest.getId());
        if(optionalAccount.isPresent()){
            Account loggedAccount = optionalAccount.get();
            loggedAccount = updateAccountProfileService.update(profileRequest, loggedAccount);

            /*
             * Upload multipart image profile
             */
            if (null != profileImg){
                Media media = saveMediaProfileAccountService.save(profileImg, loggedAccount);
            }
            return new ResponseEntity<>(new ApiResponse(true, "Account info saved!", "", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(new ApiResponse(false, "An error occured, unable to update the account associated with the email "+profileRequest.getEmail(), "", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
}
