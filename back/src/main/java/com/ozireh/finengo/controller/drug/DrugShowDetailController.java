package com.ozireh.finengo.controller.drug;

import com.ozireh.finengo.model.Drug;
import com.ozireh.finengo.services.drug.GetByIdDrugService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DrugShowDetailController {
	final
	 GetByIdDrugService getByIdDrugService;

	public DrugShowDetailController(GetByIdDrugService getByIdDrugService){
		this.getByIdDrugService = getByIdDrugService;
	}

	@GetMapping("/api/drug/detail/{id}")
	public ResponseEntity<Drug> execute(@PathVariable("id") Long id) {
		Drug drug = this.getByIdDrugService.execute(id);
		return new ResponseEntity<>(drug, HttpStatus.OK);
	}
}
