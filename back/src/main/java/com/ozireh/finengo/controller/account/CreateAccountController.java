package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.requests.AccountRequest;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.services.account.CreateAccountService;
import com.ozireh.finengo.services.account.ValidateSignUpRequestService;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;

@RestController
@RequestMapping("/api/auth")
public class CreateAccountController {
    private final CreateAccountService createAccountService;
    private final ValidateSignUpRequestService validateAccountRequest;

    public CreateAccountController(CreateAccountService createAccountService, ValidateSignUpRequestService validateAccountRequest){
        this.createAccountService = createAccountService;
        this.validateAccountRequest = validateAccountRequest;

    }

    @PostMapping("/create-account")
    public ResponseEntity<?> createAccount(@Valid @RequestBody AccountRequest signUpRequest) throws FileUploadException, IOException {
        ApiResponse response = validateAccountRequest.validate(signUpRequest);
        if(!response.getSuccess()){
            return new ResponseEntity<>(response, response.getHttp_status());
        }
        Account account = this.createAccountService.create(signUpRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(account.getUsername()).toUri();

        return ResponseEntity.created(location).body(response);
    }
}
