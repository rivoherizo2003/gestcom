package com.ozireh.finengo.controller.employee;

import com.ozireh.finengo.model.Employee;
import com.ozireh.finengo.payloads.requests.EmployeeRequest;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.repository.EmployeeRepository;
import com.ozireh.finengo.services.employee.UpdateEmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UpdateEmployeeController {
    private final EmployeeRepository employeeRepository;
    private final UpdateEmployeeService updateEmployeeService;

    public UpdateEmployeeController(EmployeeRepository employeeRepository, UpdateEmployeeService updateEmployeeService){
        this.employeeRepository = employeeRepository;
        this.updateEmployeeService = updateEmployeeService;
    }

    @PutMapping("/update/employee")
    public ResponseEntity<?> updateEmployee(@RequestBody EmployeeRequest employeeRequest, @RequestParam("id") Long id){
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if(employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            employee = updateEmployeeService.update(employeeRequest, employee);

            return new ResponseEntity<>(new ApiResponse(true, "Employee updated", "",HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(new ApiResponse(false, "Account info", "", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
}
