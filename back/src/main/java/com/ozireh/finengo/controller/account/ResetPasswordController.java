package com.ozireh.finengo.controller.account;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.payloads.responses.ApiResponse;
import com.ozireh.finengo.payloads.requests.ResetPasswordRequest;
import com.ozireh.finengo.repository.AccountRepository;
import com.ozireh.finengo.security.JwtTokenProvider;
import com.ozireh.finengo.services.account.SaveResetPasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/security")
public class ResetPasswordController {
    private final SaveResetPasswordService handleResetPasswordRequest;
    private final AccountRepository accountRepository;

    public ResetPasswordController(AccountRepository accountRepository, JwtTokenProvider jwtTokenProvider, SaveResetPasswordService handleResetPasswordRequest) {
        this.accountRepository = accountRepository;
        this.handleResetPasswordRequest = handleResetPasswordRequest;
    }

    @PatchMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {
        Optional<Account> ac = accountRepository.findByUsernameOrEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (ac.isPresent()) {
            handleResetPasswordRequest.resetPassword(ac.get(), resetPasswordRequest);

            return new ResponseEntity<>(new ApiResponse(true, "Votre mot de passe a été réinitialisé avec succés", "password", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(new ApiResponse(true, "Impossible de reinitialisé votre mot de passe, aucun compté associé à ce mail!", "password", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }
}
