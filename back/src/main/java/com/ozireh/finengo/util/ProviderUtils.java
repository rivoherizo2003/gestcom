package com.ozireh.finengo.util;

import com.ozireh.finengo.enumerator.ProviderEnum;

public class ProviderUtils {
    public static ProviderEnum valueOfLabel(String value){
        for (ProviderEnum p :
                ProviderEnum.values()) {
            if(p.toString().equals(value)) return p;
        }

        return null;
    }
}
