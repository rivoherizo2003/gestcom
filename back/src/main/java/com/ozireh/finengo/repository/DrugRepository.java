package com.ozireh.finengo.repository;

import com.ozireh.finengo.model.Drug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugRepository extends JpaRepository<Drug, Long>{

}
