package com.ozireh.finengo.repository;

import com.ozireh.finengo.model.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<Media, Long> {

}
