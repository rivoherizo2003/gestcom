package com.ozireh.finengo.repository;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.services.projection.AccountInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query(value = "SELECT a FROM Account a JOIN FETCH a.roles WHERE a.username = ?1 or a.email = ?1")
    Optional<Account> findByUsernameOrEmail(String usernameOrEmail);

    @Query(value = "SELECT a FROM Account a JOIN FETCH a.roles WHERE a.email = ?1")
    Optional<Account> findByEmail(String email);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Boolean existsAccountByEmailAndIdNot(String email, Long id);

    Boolean existsAccountByUsernameAndIdNot(String username, Long id);

    @Query( value = "SELECT account.id as id, account.createdAt as createdAt, account.updatedAt as updatedAt, " +
            " account.userCode as userCode , account.email as email, account.username as username, " +
            " media.realFileName as realFileName, role.name as roleName " +
            " FROM Account account " +
            " LEFT JOIN Media media ON media = account.media " +
            " JOIN account.roles role")
    Page<AccountInfo> findAllUsersAndPaginate(Pageable pageable);
}
