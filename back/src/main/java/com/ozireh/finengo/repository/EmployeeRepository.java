package com.ozireh.finengo.repository;

import com.ozireh.finengo.model.Account;
import com.ozireh.finengo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(value = "SELECT e FROM Employee e WHERE e.account = ?1")
    Optional<Employee> findByAccount(Account account);
}
