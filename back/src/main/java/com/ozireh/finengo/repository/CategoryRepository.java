package com.ozireh.finengo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ozireh.finengo.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{

}
