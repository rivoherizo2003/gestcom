package com.ozireh.finengo.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ozireh.finengo.model.Role;
import com.ozireh.finengo.enumerator.RoleEnum;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleEnum roleName);

	@Query(value = "select * from gc_roles where role_name IN(:roles)", nativeQuery = true)
	HashSet<Role> findNames(@Param("roles") List<String> roleNames);

	@Query(value = "select role_name from gc_roles", nativeQuery = true)
	HashSet<RoleEnum> findRoleNames();
}
 