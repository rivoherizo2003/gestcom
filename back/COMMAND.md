# Remove dangling image
```bash
`docker rmi $(docker images -f "dangling=true" -q)`
```
<hr>

## BUILD && RUN finengo
launch this command to build image and launch container:
```bash
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build
```
<hr>

### Launch
```bash
docker-compose start
```
-Front: http://localhost:3001

<hr>

## Copy data to the container gc_db
```bash
docker cp $path_csv/201701scripts_sample.csv id_container:/var/lib/postgresql/data
```

<hr>

## Launch this command to import data in the database, in the container gc_db
```bash
COPY drug(code, bnf_code, name, items, nic, price, quantity, discr, notice, expiration_date)
    FROM '/var/lib/postgresql/data/201606scripts_sample.csv'
    DELIMITER ','
    CSV HEADER;
```

# Generate keystore
```bash
keytool -genkeypair -alias finengo -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore finengo.p12 -validity 3650
```
<hr>

