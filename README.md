# finingo
A provincial name for a species of pigeon.
The Madagascar blue pigeon (Alectroenas madagascariensis) is a species of bird in the family Columbidae. The species is closely related to the other two extant species of blue pigeon, the Comoros blue pigeon and the Seychelles blue pigeon. It is endemic to northern and eastern Madagascar.

<hr>

## Techno
- React 18.2.0 with typescript
- Spring 5.2.6.RELEASE
- openjdk:17-jdk-alpine
- postgres:12-alpine

<hr>
I used these tutorials:

- https://www.callicoder.com/spring-boot-spring-security-jwt-mysql-react-app-part-1/ to set up authentication;

- https://www.callicoder.com/spring-boot-security-oauth2-social-login-part-1/ to implement OAuth2 with facebook and google;

<hr>
N.B: You can find some helpful commands in COMMAND.md
